#version 330 core

//Input Vertex Data
layout(location = 0) in vec3 vertexPosition_modelspace; //Different for all executions

//Model View Projection matrix
uniform mat4 MVP; //Same for all executions (this mesh)

void main() {
	//Transform vertice
	gl_Position = MVP * vec4(vertexPosition_modelspace, 1); //1 on the end to signify position
}
