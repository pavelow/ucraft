#version 330 core

//Input Vertex Data
layout(location = 0) in vec3 vertexPosition_modelspace; //Different for all executions
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in uint vertexType;
layout(location = 3) in vec3 vertexNormal;

//Output data, will be interpolated for each fragment
out vec2 UV;
out vec4 Normal;
flat out uint type;

//Model View Projection matrix
uniform mat4 MVP; //Same for all executions (this mesh)

void main() {
	type = vertexType; //Pass type through

	//Transform vertice
	gl_Position = MVP * vec4(vertexPosition_modelspace, 1); //1 on the end to signify position
	//Normal = MVP * vec4(vertexNormal, 0);
	Normal = vec4(vertexNormal, 0);

	//Pass UV through
	UV = vertexUV;
}
