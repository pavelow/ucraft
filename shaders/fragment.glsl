#version 330 core
in vec2 UV;
in vec4 Normal;
flat in uint type;

out vec4 color;
uniform sampler2DArray TextureSampler;
uniform vec3 CameraNormal;

void main() {
	float NormalMod = dot(normalize(Normal.xyz), normalize(CameraNormal)); //i.e. light is from direction of camera

	NormalMod = clamp(NormalMod, 0.5, 1);

	color.xyz = texture(TextureSampler, vec3(UV, type)).xyz*NormalMod;
	color.a = texture(TextureSampler, vec3(UV, type)).a; //We don't want alpha to be modified by lights
}
