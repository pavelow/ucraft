# μCraft
A Minecraft clone written in C++

## Build
Depends on

- glfw3 - [Graphics Library Framework (Windowing/GL Context)](https://www.glfw.org/)
- glew - [The OpenGL Extension Wrangler Library](http://glew.sourceforge.net/)
- glm - [OpenGL Mathematics](https://glm.g-truc.net/0.9.9/index.html)
- freetype-gl [Freetype GL - A C OpenGL Freetype engine](https://github.com/rougier/freetype-gl)
- OpenGL 3.3
- [Cmake](https://cmake.org/)

### Linux

Tested with GCC 8.2.1.
To build on linux
- Install libraries/toolchain
    - Arch Linux `sudo pacman -S base-devel cmake glm freetype2 glfw-x11 glew`
    - Debian/Ubuntu Linux `sudo apt-get install build-essential cmake libglm-dev libfreetype6-dev libglfw3-dev libglew-dev`
- Clone: `git clone git@gitlab.com:pavelow/ucraft.git --recurse`
- Compile: `./compile.sh`
- Run: `./build/uCraft`

### Windows

Tested with Visual Studio 2019, MSVC 14.21.27702
- Install VS2019 with C++ Development components
    - C++/CLI support
    - C++ CMake tools for Windows
- Install git
- Clone: `git clone git@gitlab.com:pavelow/ucraft.git --recurse`
- Install vcpkg
    -  `git clone https://github.com/Microsoft/vcpkg.git`
    -  Set environment variable `VCPKG_DEFAULT_TRIPLET=x64-windows` (Because vcpkg defaults to 32bit)
    -  `cd vcpkg`
    -  run `.\bootstrap-vcpkg.bat`
    -  With admin shell run `.\vcpkg.exe integrate install` (adds vcpkg path to VS2019)
    -  `vcpkg.exe install glm glew glfw3 freetype`
-  Launch VS2019 -> "Open a local folder" CMake should run automatically
    -  Build->Build All
    -  Run (Hopefully)


## Config
The executable expects to be in the same folder as `config.ini`.  
Configuration options:
```ini
;This is the μCraft configuration file

[voxel]
scale=1.0    ;Voxel side length, _MUST_ have decimal point in number
chunksize=16 ;Chunk side length (chunk contains voxels), max is 255 currently
octree=256   ;Octree side length (octree contains chunks), _MUST_ be a power of 2
threads=4    ;Number of threads to generate chunks with
distance=8   ;Radius of chunks around player to generate

[graphics]
antialiasing=0 ; {0,2,4,8,16,32}
fullscreen=0   ; {0, 1}
vsync=1        ; {0, 1, 2..} Sync rendering with 1/vsync*refresh rate
width=800      ; viewport width
height=600     ; viewport height
```

## Current Features
- Infinitely large Voxel based map
- Actual Voxel Destruction & Placement
- Noise based Map generation
- Flying controls
- Threaded map generation
- Map generation around radius
- Variable Voxel Size
- Config File
- File format for world persistence

## Classes
- Chunk
  - A cube of voxels, can be rendered at any position or scale.
- World
  - The "World", contains a bunch of discrete chunks
  - Knows how to generate the world
- Player
  - Keeps track of the camera
  - Knows how to draw the world

## Future Development (TODO)
- Optimised Chunk Loading, based on player velocity?
- Fog to obscure draw distance limitations
- Figure out how to increase apparent draw distance whilst maintaining performance
  - Scale Based LOD system?
  - Better chunk generation system?
- Can map generation functions be made modular?
- How to do efficient texture mapping per-voxel side. (This is largely done, just need to figure out a good way to store mappings on a per type basis)
- More efficient mesh Generation, [Greedy Meshing](https://0fps.net/2012/06/30/meshing-in-a-minecraft-game/)
  - Array Textures will help here :) this is the main reason I implemented them.

## Future Development (Ideas)
- Audio
  - [OpenAL](https://www.openal.org/)
- Mining/Inventory/Ecosystem (A big one)
- Network Support
