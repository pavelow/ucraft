#! /bin/bash

echo "Check for build dir"
if [ ! -d "build" ]; then
	echo "Make build dir"
	mkdir -p build
fi

cd build

echo "Running CMAKE..."
cmake ..
echo "Running Make"
make -j$(nproc)

echo "Done"
