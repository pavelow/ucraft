// TODO, use boost preprocessor to generate this:
// https://stackoverflow.com/questions/5093460/how-to-convert-an-enum-type-variable-to-a-string
#include <cstdint>
#include "VoxelTypes.hpp"

char* VoxelString(uint8_t type) {
  switch (type) {
    case V_EMPTY:
      return "V_EMPTY";
    case V_GRASS:
      return "V_GRASS";
    case V_GRASSDIRT:
      return "V_GRASSDIRT";
    case V_DIRT:
      return "V_DIRT";
    case V_STONE:
      return "V_STONE";
    case V_SAND:
      return "V_SAND";
    default:
      return "V_NONE";
  }
}
