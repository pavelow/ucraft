#pragma once
#include "chunk/chunk.hpp"

struct ChunkRef {
	ChunkCoord Position; //Used to differentiate chunk without following the pointer
	Chunk* ChunkPointer = NULL;
};
