#pragma once
#include <glm/glm.hpp>
#include "chunk/chunk.hpp"
#include "ChunkRef.hpp"
#include "octree/octree.hpp"
#include "chunksource/ChunkSource.hpp"
#include <vector>
#include <cstdint>
#include <cmath>
#include <thread>
#include <queue>
#include <mutex>
#include <ctime>
#include <iterator>
#include <algorithm>

class GenRequest {
public:
	GenRequest(ChunkCoord PlayerChunk ,float ChunkDistance);
	GenRequest();
	ChunkCoord PlayerChunk; //Used to differentiate chunk without following the pointer
	float ChunkDistance;
};

class World {
	public:
	World(float _Scale, uint64_t _Size, unsigned int NumThreads, unsigned int CSize, ChunkSource* _Generator); //Constructor
	~World();

	//Keep in world class
	glm::vec3 GetPlayerLookAtVoxel(); //Requires player current voxel, current chunk and camera u vector
	void SetVoxel(ChunkCoord, glm::vec3, uint8_t); //ChunkCoord Current_Chunk)   //Sets Voxel with coords relative to current chunk
	uint8_t GetVoxelType(ChunkCoord Chunk, glm::vec3 VoxelCoord);

	//Ray casting
	std::tuple<glm::vec3, glm::vec3> GetNextVoxelIntersection(glm::vec3, glm::vec3);
	glm::vec3 GetNextNonEmptyRecurse(glm::vec3 CamDir, ChunkCoord PlayerChunk, glm::vec3 PositionInVoxel, glm::vec3 VoxelCoord, int Distance, glm::vec3 PlayerVoxel, bool LastAir);

	//Eventually needs to be moved to derived class
	void DrawChunks(glm::mat4 m);  //Draw loaded/generated chunks that are in front of player


	void GenChunksAroundPlayer(ChunkCoord PlayerChunk, float ChunkDistance);//glm::vec3 PlayerCoord); //Starts chunk generation around player, needs player position and render distance
	void DelChunks(ChunkCoord Chunk, float Distance); //Delete chunks too far from player, needs player position and render distance
	void GenChunk(ChunkCoord PlayerChunk);  //Add chunk to queue

	uint64_t GetNChunks();	   //Get current number of chunks allocated
	uint64_t GetWaitingChunks(); //Get current number of chunks allocated but waiting to be generated

	int RenderedChunks;	 //When we render chunks, this variable counts how many of the allocated ones we render
	uint8_t ChunkSize = 16;
	float VoxelScale;

  private:
	void AddChunk(ChunkCoord position); //Add new chunk at (x,y,z)
	void GenRequiredChunks();//glm::vec3 PlayerCoord); //Starts chunk generation around player, needs player position and render distance

	std::vector<ChunkRef> ChunkVector; //Vector of ChunkRefs
	Octree<ChunkRef> Chunks;
	std::mutex ChunkOctree;
	std::mutex ChunkVec;

	ChunkSource* Generator;

	uint64_t OctreeSize;
	//Wrappers around Octree
	void SetVoxelIfExists(ChunkCoord position, glm::vec3 voxel, uint8_t type);
	ChunkRef GetChunk(ChunkCoord position);
	void SetChunk(ChunkCoord position, ChunkRef Chunk);
	void EraseChunk(ChunkCoord position);
	inline ChunkCoord OctreeSpace(ChunkCoord WorldSpace);

	//Threading things
	void ComputeChunk();		//Get Next Chunk from queue, compute
	//void ComputeChunkRecurse(ChunkCoord Chunk); //Figure out what chunks to add to queue ---TODO---
	bool running;
	std::vector<std::thread> GenerationThreadPool;
	std::mutex WChunks;
	std::queue<ChunkRef> WantedChunks;   //Queue of Chunks to make
	std::mutex DChunks;
	std::queue<Chunk*> ChunksToDelete;   //Queue of Chunks to delete

	std::mutex GChunks;
	std::queue<GenRequest> GRequiredChunks;   //Queue of Chunks for which to figure out what chunks to make
};
