#pragma once
#include "../../ChunkSource.hpp"
#include "../../../octree/octree.hpp"
#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

#pragma pack(push, 1)
//QfHb
struct ucHeader {
	uint8_t Magic[4];
	uint64_t OctreeSize;
	float VoxelScale;
	uint16_t TableSize;
	uint8_t ChunkSize;
};

//QQQQ
struct tableEntry {
	uint64_t x;
	uint64_t y;
	uint64_t z;
	uint64_t offset;
};
#pragma pack(pop)

class ucFile: public ChunkSource {
public:
	ucFile(std::string filename, uint64_t octreeSize, uint8_t chunkSize, float VoxelScale, uint16_t TableSize); //Creates new file
	ucFile(std::string filename); //Opens file
	~ucFile();

	bool GetChunk(ChunkRef* Reference); //Retrive chunk, return null pointer in ChunkRef if not exists in file
	bool ChunkExists(ChunkRef* Reference); //Check if chunk exists in file
	bool SaveChunk(ChunkRef Chunk); //Saves Chunk, overwrites if already exists
	uint64_t GetNChunks(); // Get number of chunks parsed
	bool Ready(); // Did a file get opened correctly?

	uint64_t TableSize;
	float VoxelScale;
	uint8_t ChunkSize;
	uint8_t MemoryChunkSize;

private:
	inline ChunkCoord OctreeSpace(ChunkCoord WorldSpace);
	int64_t mOctreeSize;

	bool isLittleEndian(); // Finds out byte order of system
	Octree<uint64_t>* ChunkOffsets = NULL; //Data structure to store/search for offsets in file
	ucHeader header;
	std::FILE* fp = NULL;
	uint64_t pow(uint64_t, uint64_t);

	uint64_t nTableEntries = 0;

	uint64_t nextTableOffset; //offset in file of next empty table entry
	uint64_t nextChunkOffset; //offset in file of next empty Chunk entry
};
