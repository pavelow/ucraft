#! /usr/bin/python
import struct

class Chunk:
	def __init__(self, Coord):
		self.Coord = Coord
		self.data = bytes(256)

class ChunkCoord:
	def __init__(self, x,y,z):
		self.x = x
		self.y = y
		self.z = z

	def ToString(self):
		return "{0},{1},{2}".format(self.x, self.y, self.z)

class ucFormat:
	def __init__(self):
		self.headerFmt = "<bbbbQfHb"
		self.tableFmt = "<QQQQ"
		self.magic = b"uc00"
		self.offsets = {}
		self.tableSize = 256 # Entries
		self.chunkSize = 17 # bytes
		self.nextTableOffset = struct.calcsize(self.headerFmt) # header size
		self.nextChunkOffset = struct.calcsize(self.headerFmt) + 256*struct.calcsize(self.tableFmt) #header size + table size +1
		self.nTableEntries = 0

	def __del__(self):
		self.f.close()


	def open(self, filename):
		self.filename = filename
		self.f = open(filename, 'rb+')
		self.f.seek(0)
		self.headerBytes = self.f.read(struct.calcsize(self.headerFmt))

		header = struct.unpack(self.headerFmt, self.headerBytes)
		self.octreeSize = header[0+4]
		self.voxelScale = header[1+4]
		self.chunkSize = header[3+4]
		self.chunkOffset = self.chunkSize**3 # How many bytes in chunk

		# Populate structure
		self.f.seek(struct.calcsize(self.headerFmt))
		while(True):
			#print("whee")
			ChunkrefBytes = struct.unpack(self.tableFmt, self.f.read(struct.calcsize(self.tableFmt)))
			if ChunkrefBytes[3] == 0: #Can't have a zero offset if it is valid
				break
			Coord = ChunkCoord(ChunkrefBytes[0], ChunkrefBytes[1], ChunkrefBytes[2])
			self.offsets[Coord.ToString()] = ChunkrefBytes[3]
			self.nTableEntries = self.nTableEntries + 1
			self.nextTableOffset = self.nextTableOffset + struct.calcsize(self.tableFmt)
			if(self.nTableEntries % self.tableSize == 0):
				print("R:Old last @ {0}".format(hex(self.nextTableOffset)))
				self.nextTableOffset += self.chunkOffset * self.tableSize # Number of chunk sizes futher into file
				self.nextChunkOffset += self.tableSize * struct.calcsize(self.tableFmt)
				print("R:New table @ {0}".format(hex(self.nextTableOffset)))
				self.f.seek(self.nextTableOffset)

		self.nextTableOffset = self.nextTableOffset + self.nTableEntries * struct.calcsize(self.tableFmt)
		self.nextChunkOffset = (self.chunkOffset * self.nTableEntries) + self.nextChunkOffset
		#nextTableOffset = floor(nTableEntries/tableSize)      nextTableOffset +  (nTableEntries) %


	def new(self, filename, octreeSize, chunkSize, voxelScale):
		self.filename = filename
		self.f = open(filename, 'wb+')
		self.octreeSize = octreeSize
		self.chunkSize = chunkSize
		self.voxelScale = voxelScale
		self.chunkOffset = chunkSize**3 #How many bytes in chunk
		headerBytes = struct.pack(self.headerFmt, self.magic[0], self.magic[1], self.magic[2], self.magic[3], octreeSize, voxelScale, self.tableSize, chunkSize)
		self.f.seek(0)
		self.f.write(headerBytes)
		#header is written to new file

	#function saves a chunk
	def Write(self, Chunk):
		if(Chunk.Coord.ToString() in self.offsets):
			#print("overwrite")
			self.f.seek(self.offsets[Chunk.Coord.ToString()])
			self.f.write(Chunk.data)
		else: # We need to allocate new table entries
			#print("new")
			self.offsets[Chunk.Coord.ToString()] = self.nextChunkOffset
			ChunkTableBytes = struct.pack(self.tableFmt, Chunk.Coord.x, Chunk.Coord.y, Chunk.Coord.z, self.nextChunkOffset)

			#Write Table entry
			self.f.seek(self.nextTableOffset)
			self.f.write(ChunkTableBytes)

			#Calculate new offsets
			self.nextTableOffset = self.nextTableOffset + struct.calcsize(self.tableFmt) #tableSize, will need to modify when extended tables are used
			#print(self.nextTableOffset)
			self.nextChunkOffset = self.nextChunkOffset + self.chunkOffset

			self.nTableEntries = self.nTableEntries + 1

			#Calculate if we need to start a new table
			if(self.nTableEntries % self.tableSize == 0):
				print("W:Old last @ {0}".format(hex(self.nextTableOffset)))
				self.nextTableOffset = self.nextTableOffset + (self.chunkOffset * self.tableSize)
				self.nextChunkOffset = self.nextChunkOffset + (self.tableSize * struct.calcsize(self.tableFmt))
				print("W:New table @ {0}".format(hex(self.nextTableOffset)))

			#Wrrite Chunk Data
			#print(hex(self.offsets[Chunk.Coord.ToString()]))
			self.f.seek(self.offsets[Chunk.Coord.ToString()])
			self.f.write(Chunk.data)

	def Read(self, ChunkCoord):
		#find in datastructure
		self.f.seek(self.offsets[ChunkCoord.ToString()])
		#Pull out of file
		data = self.f.read(self.chunkOffset)
		return data

if __name__ == '__main__':
	# Create file
	a = ucFormat()
	#a.open("test3.uc")
	a.new("test3.uc", 65536, 16, 1.0)

	c = Chunk(ChunkCoord(65535, 42,42))
	str = "Hello World"
	c.data = bytearray(str, 'utf-8')
	a.Write(c)

	for i in range(1024):
		# Create Chunk object
		c = Chunk(ChunkCoord(1*i,2*i,3*i))
		a.Write(c)

	del a
	# --read--
	b = ucFormat()
	b.open("test3.uc")

	for i in range(1024):
		c = b.Read(ChunkCoord(1*i,2*i,3*i))

	print(b.Read(ChunkCoord(65535, 42,42)))
