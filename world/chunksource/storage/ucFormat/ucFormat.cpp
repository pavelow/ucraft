#include "ucFormat.hpp"

// Create file
ucFile::ucFile(std::string filename, uint64_t octreeSize, uint8_t _ChunkSize, float _VoxelScale, uint16_t _TableSize) {//}: ChunksInFile(octreeSize) {  //Opens file, or creates new one
	size_t ret;

	// Build Header
	struct ucHeader header;
	memcpy(header.Magic, "uv00", 4);
	header.OctreeSize = octreeSize;
	header.VoxelScale = _VoxelScale;
	header.TableSize = _TableSize;
	header.ChunkSize = _ChunkSize;

	mOctreeSize = octreeSize;

	printf("Octree: %d\n", octreeSize);
	printf("Size: %d\n", _ChunkSize);

	TableSize = _TableSize;
	ChunkSize = _ChunkSize;
	MemoryChunkSize = _ChunkSize+2;

	// Make sure file doesn't exist
	fp = std::fopen(filename.c_str(), "r");
	if ( fp != NULL ) {
		printf("File already exists!\n");
		std::fclose(fp);
		fp = NULL;
		return;
	}

	// Create file
	fp = std::fopen(filename.c_str(), "w+b");

	// Write header
	ret = std::fseek(fp, 0, SEEK_SET);
	if(ret)
		return;
	ret = std::fwrite(&header, sizeof(struct ucHeader), 1, fp);
	if(!ret)
		return;

	// Create octree
	ChunkOffsets = new Octree<uint64_t>(header.OctreeSize);

	// Setup offsets
	nextTableOffset = sizeof(struct ucHeader);
	nextChunkOffset = sizeof(struct ucHeader) + TableSize*sizeof(struct tableEntry);
}

// Open file
ucFile::ucFile(std::string filename) {
	size_t ret;

	if(!isLittleEndian()) {
		printf("Wrong endianness, conversion not yet implemented!\n");
		return;
	}

	printf("Open %s\n", filename.c_str());
	fp = std::fopen(filename.c_str(), "r+b");
	if(fp == NULL) {
		printf("Error reading file, does it exist?\n");
		return;
	}
	std::fseek(fp, 0, SEEK_SET);

	// Read header into struct;
	ret = std::fread(&header, 1, sizeof(ucHeader), fp);

	printf("Octree Size: %d\n", header.OctreeSize);
	mOctreeSize = header.OctreeSize;

	// Create octree with size from header value
	ChunkOffsets = new Octree<uint64_t>(header.OctreeSize);
	ChunkSize = header.ChunkSize;
	MemoryChunkSize = header.ChunkSize + 2; //Due to overlap in chunks
	VoxelScale = header.VoxelScale;
	TableSize = header.TableSize;

	//Read table into octree
	tableEntry entry;
	for(;;) {
		ret = std::fread(&entry, sizeof(tableEntry), 1, fp);
		nTableEntries++;

		if(nTableEntries % TableSize == 0) {
			nextTableOffset = sizeof(ucHeader) + nTableEntries*sizeof(tableEntry) + (nTableEntries * pow(MemoryChunkSize,3));
			std::fseek(fp, nextTableOffset, SEEK_SET);
		}

		// If we're at the last table entry, we're done!
		if (entry.offset == 0 || ret == 0) {
			nTableEntries--;
			nextTableOffset = ftell(fp) - sizeof(struct tableEntry);
			break; //octree is populated!
		} else {
			printf("Found: Octree:%d:%d:%d @ %d\n", entry.x, entry.y, entry.z, entry.offset);
			ChunkOffsets->Set(entry.x, entry.y, entry.z, entry.offset);
			nextChunkOffset = entry.offset + pow(MemoryChunkSize,3); //Next chunk data offset
		}
	}
}

ucFile::~ucFile() {
	delete ChunkOffsets;
	if (fp != NULL)
		std::fclose(fp);
}

uint64_t ucFile::pow(uint64_t value, uint64_t pwr) {
	uint64_t ret = value;
	for(uint64_t i = 0; i < pwr-1; i++) {
		ret *= value;
	}
	return ret;
}

uint64_t ucFile::GetNChunks() {
	return nTableEntries;
}

bool ucFile::GetChunk(ChunkRef* Reference) { //Retrive chunk, return null pointer in ChunkRef if not exists in file
	// printf("GetChunk %d %d %d\n", Reference->Position.x, Reference->Position.y, Reference->Position.z);

	if( fp == NULL )
		return false;

	ChunkCoord ChunkPos = OctreeSpace(Reference->Position);
	// printf("Octree: %d %d %d\n", ChunkPos.x, ChunkPos.y, ChunkPos.z);

	if(!ChunkExists(Reference)) {
		return false;
	}

	int ret;

	//Get offset from octree
	uint64_t offset = ChunkOffsets->Get(ChunkPos.x, ChunkPos.y, ChunkPos.z);

	//Construct Chunk
	Reference->ChunkPointer->Exists = true;
	Reference->ChunkPointer->State = Chunk::S_VGENED;
	Reference->ChunkPointer->position = Reference->Position;

	//Read data
	ret = std::fseek(fp, offset, SEEK_SET);
	if (ret)
		return false;

	std::fread(Reference->ChunkPointer->Voxels , pow(MemoryChunkSize,3), 1, fp);
	if (ret)
		return false;

	return true;
}

bool ucFile::ChunkExists(ChunkRef* Reference) {  //Check if chunk exists in file
	ChunkCoord CPos = Reference->Position;
	// printf("%d:%d:%d\n", CPos.x, CPos.y, CPos.z);
	ChunkCoord ChunkPos = OctreeSpace(CPos);
	// printf("%d:%d:%d\n", ChunkPos.x, ChunkPos.y, ChunkPos.z);

	if( fp == NULL )
		return false;
	// printf("B\n");
	return ChunkOffsets->Exists(ChunkPos.x, ChunkPos.y, ChunkPos.z);
}

bool ucFile::SaveChunk(ChunkRef Chunk) { //Saves Chunk, overwrites if already exists
	printf("S:%d:%d:%d\n", Chunk.Position.x, Chunk.Position.y, Chunk.Position.z);
	if( fp == NULL )
		return false;

	size_t ret;

	ChunkCoord OctreeCoord = OctreeSpace(Chunk.Position);
	// printf("A\n");

	//Check if exists, overwrite
	if(ChunkExists(&Chunk)) {
		uint64_t offset = ChunkOffsets->Get(OctreeCoord.x, OctreeCoord.y, OctreeCoord.z);
		printf("Chunk exists, overwrite @%d\n", offset);

		ret = std::fseek(fp, offset, SEEK_SET);
		if(ret)
			return false;

		ret = std::fwrite(Chunk.ChunkPointer->Voxels, pow(MemoryChunkSize,3), 1, fp);
		if(ret)
			return false;

	} else {

		//if not exists, create new entry
		struct tableEntry entry;
		//Fill out struct
		entry.x = OctreeCoord.x;
		entry.y = OctreeCoord.y;
		entry.z = OctreeCoord.z;

		// Check if we're at the end of a table and alter offsets accordingly
		if(nTableEntries != 0 && nTableEntries % TableSize == 0) {
			nextTableOffset = sizeof(ucHeader) + nTableEntries*sizeof(tableEntry) + (nTableEntries*pow(MemoryChunkSize,3));
			nextChunkOffset += nTableEntries*sizeof(tableEntry);
		}
		entry.offset = nextChunkOffset;
		printf("Chunk doesn't exist, new entry: %d:%d:%d @ %lu\n", entry.x, entry.y, entry.z, nextChunkOffset);

		//Write table entry
		ret = std::fseek(fp, nextTableOffset, SEEK_SET);
		if(ret)
			return false;

		ret = std::fwrite(&entry, sizeof(tableEntry), 1, fp);
		if(!ret)
			return false;

		nextTableOffset += sizeof(tableEntry);

		//Write data
		ret = std::fseek(fp, nextChunkOffset, SEEK_SET);
		if(ret)
			return false;
		ret = std::fwrite(Chunk.ChunkPointer->Voxels, pow(MemoryChunkSize,3), 1, fp);
		if(!ret)
			return false;

		nextChunkOffset += pow(MemoryChunkSize,3);

		// Add to octree
		ChunkOffsets->Set(entry.x, entry.y, entry.z, entry.offset);
	}
	return true;
}

bool ucFile::isLittleEndian() {
	uint16_t num = 1;
	if (*(uint8_t *)&num == 1)
		return true;
	return false;
}

inline ChunkCoord ucFile::OctreeSpace(ChunkCoord WorldSpace) {
	uint64_t Sz = mOctreeSize/2;
	ChunkCoord OctSpace = ChunkCoord(WorldSpace.x + Sz, WorldSpace.y + Sz, WorldSpace.z + Sz);
	return OctSpace;
}

bool ucFile::Ready() {
	if (fp == NULL)
		return false;
	return true;
}
