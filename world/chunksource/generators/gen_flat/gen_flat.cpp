#include "gen_flat.hpp"

gen_flat::gen_flat(float VoxelScale, uint8_t ChunkSize) : ChunkSource(VoxelScale, ChunkSize) {}
gen_flat::~gen_flat() {}

bool gen_flat::GetChunk(ChunkRef* Reference) {
	ChunkCoord ChunkPos = Reference->Position;

	uint8_t maxVoxel = 0;
	for(int x= -1; x < ChunkSize + 1; x++) {
		for(int y= -1; y < ChunkSize + 1; y++) {
			for(int z= -1; z < ChunkSize + 1; z++) {

				/* Compute Absolute Positions */
				int64_t yA = y+ChunkPos.y*ChunkSize;
				Reference->ChunkPointer->Voxels[Offset(x,y,z)] = 0;

				/* Saves extra checks of other voxels */
				if(yA < 4) {
					if(yA > 2)
						Reference->ChunkPointer->Voxels[Offset(x,y,z)] = V_GRASSDIRT;
					if(yA < 3)
						Reference->ChunkPointer->Voxels[Offset(x,y,z)] = V_DIRT;
					if(yA < 1)
						Reference->ChunkPointer->Voxels[Offset(x,y,z)] = V_STONE;
					if (Reference->ChunkPointer->Voxels[Offset(x,y,z)] > maxVoxel)
						maxVoxel = Reference->ChunkPointer->Voxels[Offset(x,y,z)];
				}
			}
		}
	}

	if(maxVoxel > 0) {
		Reference->ChunkPointer->State = Chunk::S_VGENED;
	} else {
		Reference->ChunkPointer->State = Chunk::S_EMPTY;
	}

	return true;
}


inline unsigned gen_flat::Offset(int x, int y, int z) {
	unsigned Sz = ChunkSize+2;
	unsigned offset = ((x+1)*(Sz*Sz))+((y+1)*Sz+(z+1));
	return offset;
}
