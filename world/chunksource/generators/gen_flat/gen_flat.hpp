#pragma once
#include "../../ChunkSource.hpp"
#include "../../../octree/octree.hpp"
#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

class gen_flat: public ChunkSource {
public:
	gen_flat(float VoxelScale, uint8_t ChunkSize); //Creates new file
	~gen_flat();

	inline unsigned Offset(int x, int y, int z);

	/* Retrive chunk, return null pointer if fails */
	bool GetChunk(ChunkRef* Reference);
};
