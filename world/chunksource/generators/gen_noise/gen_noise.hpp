#pragma once
#include "../../ChunkSource.hpp"
#include "../../../octree/octree.hpp"
#include "../../FastNoise/FastNoise.h"
#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

class gen_noise: public ChunkSource {
public:
	gen_noise(float VoxelScale, uint8_t ChunkSize); //Creates new file
	~gen_noise();

	inline unsigned Offset(int x, int y, int z);

	/* Retrive chunk, return null pointer if fails */
	bool GetChunk(ChunkRef* Reference);
};
