#include "gen_noise.hpp"

gen_noise::gen_noise(float VoxelScale, uint8_t ChunkSize) : ChunkSource(VoxelScale, ChunkSize) {}
gen_noise::~gen_noise() {}

bool gen_noise::GetChunk(ChunkRef* Reference) {
	ChunkCoord ChunkPos = Reference->Position;
	FastNoise Noise;
	Noise.SetNoiseType(FastNoise::SimplexFractal);

	//Generate Map
	int Empty = 0; //Check if there is air.
	int voxels = 0; //Check if there are any voxels
	for(int x=-1; x < ChunkSize+1; x++) {
		float xA = (float)(x+ChunkPos.x*ChunkSize)*VoxelScale;
		for(int z=-1; z < ChunkSize+1; z++) {
			float zA = (float)(z+ChunkPos.z*ChunkSize)*VoxelScale;
			for(int y=-1; y < ChunkSize+1; y++) {
				float yA = (float)(y+ChunkPos.y*ChunkSize)*VoxelScale;

				uint32_t offset = Offset(x,y,z);
				Reference->ChunkPointer->Voxels[offset] = 0;

				if (yA < 0) {
					float N = Noise.GetNoise(xA, zA, yA);
					if(N < 0.3) {
						Reference->ChunkPointer->Voxels[offset] = V_STONE;
					}
				} else {
					float N = Noise.GetNoise(xA, zA);
					float S = 32;
					if(yA < N*S) {
						Reference->ChunkPointer->Voxels[offset] = V_GRASSDIRT;
					}
					if(yA < (N*S)-1) {
						Reference->ChunkPointer->Voxels[offset] = V_DIRT;
					}
					if(yA < (N*S)-2) {
						Reference->ChunkPointer->Voxels[offset] = V_STONE;
					}

					if(yA < 1) {
						Reference->ChunkPointer->Voxels[offset] = V_SAND;
					}
				}
				uint8_t V = Reference->ChunkPointer->Voxels[offset]; //To check if this is an empty
				voxels += Reference->ChunkPointer->Voxels[offset];;
				if(V == V_EMPTY) {
					Empty++;
				}
			}
		}
	}
	if(voxels > 0 && Empty > 0) {
		Reference->ChunkPointer->State = Chunk::S_VGENED;
	}
	/*if(Empty > 0) {
		; //Gives a hint to the chunk generator
	}*/

	return true;
}


inline unsigned gen_noise::Offset(int x, int y, int z) {
	unsigned Sz = ChunkSize+2;
	unsigned offset = ((x+1)*(Sz*Sz))+((y+1)*Sz+(z+1));
	return offset;
}
