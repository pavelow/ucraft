#pragma once
#include "../chunk/chunk.hpp"
#include "../VoxelTypes.hpp"
#include "../ChunkRef.hpp"

class ChunkSource {
public:
	ChunkSource(float _VoxelScale, uint8_t _ChunkSize) {VoxelScale = _VoxelScale; ChunkSize = _ChunkSize;}
	ChunkSource() {;}
	virtual ~ChunkSource() {};
	virtual bool GetChunk(ChunkRef* Reference) = 0; //Retrive chunk, return null pointer in ChunkRef if not exists
	virtual bool SaveChunk(ChunkRef Chunk) { return false; }; 	//Saves Chunk, overwrites if already exists
	virtual bool Ready() { return true; }; 						//Check if the source is ready, in case things need to be setup
	virtual bool ChunkExists(ChunkRef* Reference) { return true; };

	float VoxelScale;
	uint8_t ChunkSize;
};
