#pragma once
#include "../../ChunkSource.hpp"
#include "../../../octree/octree.hpp"
#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <mutex>

class gen_persist: public ChunkSource {
public:
	gen_persist(float VoxelScale, uint8_t ChunkSize, ChunkSource *_Gen, ChunkSource *_Persist); //Creates new file
	~gen_persist();

	inline unsigned Offset(int x, int y, int z);

	/* Retrive chunk, return null pointer if fails */
	bool GetChunk(ChunkRef* Reference);

	/* Returns void, saving unsupported */
	bool SaveChunk(ChunkRef Chunk);

	/* Check if both sub sources are ready */
	bool Ready();

private:
	ChunkSource *Gen;
	ChunkSource *Persist;
	std::mutex PLock;
};
