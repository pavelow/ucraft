#include "gen_persist.hpp"


gen_persist::gen_persist(float VoxelScale, uint8_t ChunkSize, ChunkSource *_Gen, ChunkSource *_Persist) : ChunkSource(VoxelScale, ChunkSize) {
	Persist = _Persist;
	Gen = _Gen;
}
gen_persist::~gen_persist() {}

bool gen_persist::GetChunk(ChunkRef* Reference) {
	bool ret = true;
	if (Persist->ChunkExists(Reference)) {
		PLock.lock();
		ret = Persist->GetChunk(Reference);
		PLock.unlock();
	} else {
		ret = Gen->GetChunk(Reference);
	}
	return ret;
}

bool gen_persist::SaveChunk(ChunkRef Chunk) {
	bool ret;
	PLock.lock();
	ret = Persist->SaveChunk(Chunk);
	PLock.unlock();
	return ret;
}

bool gen_persist::Ready() {
	return Gen->Ready() && Persist->Ready();
}

inline unsigned gen_persist::Offset(int x, int y, int z) {
	unsigned Sz = ChunkSize+2;
	unsigned offset = ((x+1)*(Sz*Sz))+((y+1)*Sz+(z+1));
	return offset;
}
