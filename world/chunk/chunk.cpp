#include "chunk.hpp"

//Constructor
Chunk::Chunk(uint8_t _Size, float _Scale, ChunkCoord _position) {
	State = S_EMPTY; //Initialise empty
	Size = _Size;
	Scale = _Scale;
	position = _position;
	uint32_t size = ((uint32_t)pow(Size+2, 3) * sizeof(uint8_t));
	//printf("Size: %d\n", size);
	Voxels = (uint8_t*)malloc(size); //One Larger on each Side
	if(!Voxels) {
		printf("Malloc Failed");
		exit(0);
	}

	Exists = true;
	EmptyVoxels = false;
}

Chunk::~Chunk() {
	if(State == S_READY) {
		State = S_EMPTY; //Set to empty, in case we use threading
		glBindVertexArray(VertexArray);
		glDeleteBuffers(4, GLbuffers);
		glDeleteVertexArrays(1, &VertexArray);
	}

	free(Voxels);
}

inline unsigned Chunk::Offset(int x, int y, int z) {
	unsigned Sz = Size+2;
	unsigned offset = ((x+1)*(Sz*Sz))+((y+1)*Sz+(z+1));
	return offset;
}

uint8_t Chunk::GetVoxelType(int x, int y, int z) {
	return Voxels[Offset(x,y,z)];
}

void Chunk::SetVoxel(int x, int y, int z, uint8_t Type) {
	Voxels[Offset(x,y,z)] = Type;
	State = S_REMESH; //Set state to remesh
	GenCullMesh();		//Generate mesh
}

void Chunk::ClearMesh() {
	State = S_VGENED;
	vertices.clear();
	uvs.clear();
	normals.clear();
	types.clear();
}

//Generate Mesh, returns verticies, uvs and normals of Voxel object
void Chunk::GenCullMesh() {
	//printf("Gen Mesh %d\n", State);
	if(!(State == S_VGENED || State == S_REMESH)) { //if no voxels were generated and no remesh is needed
		return;
	}
	// ClearMesh();
	//printf("GenCullMesh S:%d Scale:%f\n", Size, Scale);
	//float start = glfwGetTime();
	//State = S_VGENED;

	for(uint8_t x=0; x < Size; x++) {
		for(uint8_t y=0; y < Size; y++) {
			for(uint8_t z=0; z < Size; z++) {
				//printf("GenMesh %d:%d:%d\n", x,y,z);
				if(Voxels[Offset(x,y,z)] != 0) {
					GenSingleVoxelMesh(x, y, z);
				}
			}
		}
	}

	//float end = glfwGetTime();
	//printf("Generated %d:%d:%d:%d Verts in %f ms\n", (int)vertices.size(), (int)uvs.size(), (int)types.size(), (int)normals.size(), 1000.0f*(end-start));


	if(State == S_REMESH) {
		State = S_MESH;
		GenBuffers();
	} else {
		if(vertices.size() > 0) {
			State = S_MESH;
		} else {
			State = S_NMESH;
		}
	}

	return;
}


//Make mesh for a single voxel
void Chunk::GenSingleVoxelMesh(int x, int y, int z) {

	uint8_t type = (uint8_t)(Voxels[Offset(x,y,z)]*16)-1; //Type with index fix
	// uint8_t TexIndex = (type*16)-1;

	//Get UV mapping
	// float Unit = 1.0f/TexMapSize;

	//u (x)
	float TexLeft	= 0;//(Type-1)*Unit;
	float TexRight = 1;//Type*Unit;
	//v (y)
	float TexTop	 = 1;//1.0f - floor(((Type-1)/TexMapSize))*Unit;
	float TexBot	 = 0;//TexTop - Unit;

	glm::vec3 normal;

	//printf("T:%f B:%f R:%f L:%f\n", TexTop, TexBot, TexRight, TexLeft);

	//Cube Verticies
	enum CubeCoords {FLB, FRB, FLT, FRT, BLB, BRB, BLT, BRT};
	glm::vec3 CubeVerts[8] = {
		glm::vec3(-1.0f,-1.0f, 1.0f),
		glm::vec3(1.0f,-1.0f, 1.0f),
		glm::vec3(-1.0f,1.0f, 1.0f),
		glm::vec3(1.0f,1.0f, 1.0f),
		glm::vec3(-1.0f,-1.0f, -1.0f),
		glm::vec3(1.0f,-1.0f, -1.0f),
		glm::vec3(-1.0f,1.0f, -1.0f),
		glm::vec3(1.0f,1.0f, -1.0f)
	};

	for(int i=0; i < 8; i++) {
		//Scale Verticies
		CubeVerts[i] *= Scale/2;
		//Translate Verticies
		CubeVerts[i] += glm::vec3(x - Size/2, y, z - Size/2) * Scale; //X
		CubeVerts[i] += float(Size)*position.Vec3()*Scale;
	}

	if(Voxels[Offset(x,y,z+1)] == 0) {

		//FrontFace
		normal = glm::vec3(0,0,-1);

		vertices.push_back(CubeVerts[FLB]); //Left Bottom
		vertices.push_back(CubeVerts[FRB]);	//Right Bottom
		vertices.push_back(CubeVerts[FLT]); //Left Top

		normals.push_back(normal); //Once
		normals.push_back(normal); //Per
		normals.push_back(normal); //Vertex :(

		uvs.push_back(glm::vec2(TexLeft, TexBot));
		uvs.push_back(glm::vec2(TexRight, TexBot));
		uvs.push_back(glm::vec2(TexLeft, TexTop));

		types.push_back(type);
		types.push_back(type);
		types.push_back(type);

		vertices.push_back(CubeVerts[FRB]); //Right Bottom
		vertices.push_back(CubeVerts[FRT]);	//Right Top
		vertices.push_back(CubeVerts[FLT]);//Left Top

		normals.push_back(normal); //Once
		normals.push_back(normal); //Per
		normals.push_back(normal); //Vertex :(

		uvs.push_back(glm::vec2(TexRight, TexBot));
		uvs.push_back(glm::vec2(TexRight, TexTop));
		uvs.push_back(glm::vec2(TexLeft,	TexTop));

		types.push_back(type);
		types.push_back(type);
		types.push_back(type);
	}

	if(Voxels[Offset(x,y,z-1)] == 0) {
		//BackFace
		normal = glm::vec3(0,0,1);
		vertices.push_back(CubeVerts[BLT]); //Left Bottom
		vertices.push_back(CubeVerts[BRB]);	//Right Bottom
		vertices.push_back(CubeVerts[BLB]); //Left Top

		normals.push_back(normal); //Once
		normals.push_back(normal); //Per
		normals.push_back(normal); //Vertex :(

		types.push_back(type);
		types.push_back(type);
		types.push_back(type);

		uvs.push_back(glm::vec2(TexLeft, TexTop));
		uvs.push_back(glm::vec2(TexRight, TexBot));
		uvs.push_back(glm::vec2(TexLeft, TexBot));

		vertices.push_back(CubeVerts[BLT]); //Right Bottom
		vertices.push_back(CubeVerts[BRT]);	//Right Top
		vertices.push_back(CubeVerts[BRB]);//Left Top

		normals.push_back(normal); //Once
		normals.push_back(normal); //Per
		normals.push_back(normal); //Vertex :(

		types.push_back(type);
		types.push_back(type);
		types.push_back(type);

		uvs.push_back(glm::vec2(TexLeft,	TexTop));
		uvs.push_back(glm::vec2(TexRight, TexTop));
		uvs.push_back(glm::vec2(TexRight, TexBot));
	}

	if(Voxels[Offset(x-1,y,z)] == 0) {
		//LeftFace
		normal = glm::vec3(1,0,0);
		vertices.push_back(CubeVerts[FLB]); //Left Bottom
		vertices.push_back(CubeVerts[BLT]); //Left Top
		vertices.push_back(CubeVerts[BLB]); //Left Back

		normals.push_back(normal); //Once
		normals.push_back(normal); //Per
		normals.push_back(normal); //Vertex :(

		types.push_back(type);
		types.push_back(type);
		types.push_back(type);

		uvs.push_back(glm::vec2(TexRight, TexBot));
		uvs.push_back(glm::vec2(TexLeft, TexTop));
		uvs.push_back(glm::vec2(TexLeft, TexBot));

		vertices.push_back(CubeVerts[FLT]);
		vertices.push_back(CubeVerts[BLT]);
		vertices.push_back(CubeVerts[FLB]);

		normals.push_back(normal); //Once
		normals.push_back(normal); //Per
		normals.push_back(normal); //Vertex :(

		types.push_back(type);
		types.push_back(type);
		types.push_back(type);

		uvs.push_back(glm::vec2(TexRight,	TexTop));
		uvs.push_back(glm::vec2(TexLeft, TexTop));
		uvs.push_back(glm::vec2(TexRight, TexBot));
}

if(Voxels[Offset(x+1,y,z)] == 0) {
	//RightFace
	normal = glm::vec3(-1,0,0);
	vertices.push_back(CubeVerts[BRB]);
	vertices.push_back(CubeVerts[BRT]);
	vertices.push_back(CubeVerts[FRB]);

	normals.push_back(normal); //Once
	normals.push_back(normal); //Per
	normals.push_back(normal); //Vertex :(

	types.push_back(type);
	types.push_back(type);
	types.push_back(type);

	uvs.push_back(glm::vec2(TexRight, TexBot));
	uvs.push_back(glm::vec2(TexRight, TexTop));
	uvs.push_back(glm::vec2(TexLeft, TexBot));

	vertices.push_back(CubeVerts[FRB]);
	vertices.push_back(CubeVerts[BRT]);
	vertices.push_back(CubeVerts[FRT]);

	normals.push_back(normal); //Once
	normals.push_back(normal); //Per
	normals.push_back(normal); //Vertex :(

	types.push_back(type);
	types.push_back(type);
	types.push_back(type);

	uvs.push_back(glm::vec2(TexLeft,	TexBot));
	uvs.push_back(glm::vec2(TexRight, TexTop));
	uvs.push_back(glm::vec2(TexLeft, TexTop));
}

if(Voxels[Offset(x,y+1,z)] == 0) {
	//TopFace
	uint8_t T = type;
	//Set Bottom Texture
	if(type == (V_GRASSDIRT*16)-1) {
		T = (V_GRASS*16)-1; //Type with index fix;
	}
	normal = glm::vec3(0,-1,0);
	vertices.push_back(CubeVerts[FLT]);
	vertices.push_back(CubeVerts[FRT]);
	vertices.push_back(CubeVerts[BRT]);

	normals.push_back(normal); //Once
	normals.push_back(normal); //Per
	normals.push_back(normal); //Vertex :(

	types.push_back(T);
	types.push_back(T);
	types.push_back(T);

	uvs.push_back(glm::vec2(TexLeft, TexBot));
	uvs.push_back(glm::vec2(TexRight, TexBot));
	uvs.push_back(glm::vec2(TexRight, TexTop));

	vertices.push_back(CubeVerts[BRT]);
	vertices.push_back(CubeVerts[BLT]);
	vertices.push_back(CubeVerts[FLT]);

	normals.push_back(normal); //Once
	normals.push_back(normal); //Per
	normals.push_back(normal); //Vertex :(

	types.push_back(T);
	types.push_back(T);
	types.push_back(T);

	uvs.push_back(glm::vec2(TexRight,	TexTop));
	uvs.push_back(glm::vec2(TexLeft, TexTop));
	uvs.push_back(glm::vec2(TexLeft, TexBot));
}

if(Voxels[Offset(x,y-1,z)] == 0) {
	uint8_t T = type;
	//Set Bottom Texture
	if(type == (V_GRASSDIRT*16)-1) {
		T = (V_DIRT*16)-1; //Type with index fix;
	}


	//Bottom Face
	normal = glm::vec3(0,1,0);
	vertices.push_back(CubeVerts[BRB]);
	vertices.push_back(CubeVerts[FRB]);
	vertices.push_back(CubeVerts[FLB]);

	normals.push_back(normal); //Once
	normals.push_back(normal); //Per
	normals.push_back(normal); //Vertex :(

	types.push_back(T);
	types.push_back(T);
	types.push_back(T);

	uvs.push_back(glm::vec2(TexRight, TexTop));
	uvs.push_back(glm::vec2(TexRight, TexBot));
	uvs.push_back(glm::vec2(TexLeft, TexBot));

	vertices.push_back(CubeVerts[FLB]);
	vertices.push_back(CubeVerts[BLB]);
	vertices.push_back(CubeVerts[BRB]);

	normals.push_back(normal); //Once
	normals.push_back(normal); //Per
	normals.push_back(normal); //Vertex :(

	types.push_back(T);
	types.push_back(T);
	types.push_back(T);

	uvs.push_back(glm::vec2(TexLeft, TexBot));
	uvs.push_back(glm::vec2(TexLeft, TexTop));
	uvs.push_back(glm::vec2(TexRight,	TexTop));
}
	return;
}

glm::mat4 Chunk::GetModelMatrix() {
	return glm::translate(glm::mat4(1.0f), position.Vec3());
}

//Draw Vobject
void Chunk::Draw() {
	if(State == S_READY || State == S_REMESH) { //If Chunk is ready (meshed, buffered)
		//printf("Draw: %d:%d:%d:%d\n", GLbuffers[0], GLbuffers[1], GLbuffers[2], GLbuffers[3]);

		glBindVertexArray(VertexArray);
		glDrawArrays(GL_TRIANGLES, 0, Verticies);

	} else if(State == S_MESH) { //If only meshed, we should buffer it
		//printf("GenBuffer\n");
		GenBuffers();
	}
}

void Chunk::GenBuffers() {
	if(State == S_MESH) { //If it's meshed
		glGenVertexArrays(1, &VertexArray);
		glBindVertexArray(VertexArray);

		if(glIsBuffer(glIsBuffer(GLbuffers[0]))) {
			glDeleteBuffers(4, GLbuffers);
		}

		glGenBuffers(4, GLbuffers);


		//Vertices Buffer
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, GLbuffers[1]);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

		//Vertex UV buffer
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, GLbuffers[0]);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW); //GL_STATIC_DRAW GL_DYNAMIC_DRAW

		//Vertex normal buffer
		glEnableVertexAttribArray(3);
		glBindBuffer(GL_ARRAY_BUFFER, GLbuffers[2]);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);

		//Vertex Texture buffer
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, GLbuffers[3]);
		glVertexAttribIPointer(2, 1, GL_UNSIGNED_BYTE, 0, 0);/**/
		glBufferData(GL_ARRAY_BUFFER, types.size(), &types[0], GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		//Clear buffers, to save memory
		Verticies = (int)vertices.size();

		/*std::vector < glm::vec3 > vertices;
		std::vector < glm::vec2 > uvs;
		std::vector < glm::vec3 > normals;
		std::vector < GLubyte > types;*/


		vertices.clear();
		vertices = std::vector<glm::vec3>();
		uvs.clear();
		uvs = std::vector<glm::vec2>();
		normals.clear();
		normals = std::vector<glm::vec3>();
		types.clear();
		types = std::vector<GLubyte>();

		State = S_READY;
	}
}

// bool ChunkCoord::operator==(ChunkCoord const& rhs) {
//	 bool eq = false;
//	 if(x == rhs.x && y == rhs.y && z == rhs.z)
//		 eq = true;
//	 return eq;
// }

ChunkCoord::ChunkCoord() {
	x = 0;
	y = 0;
	z = 0;
}

ChunkCoord::ChunkCoord(int64_t _x, int64_t _y, int64_t _z) {
	x = _x;
	y = _y;
	z = _z;
}

float ChunkCoord::distance(const ChunkCoord& distTo) {
	ChunkCoord d = ChunkCoord(x - distTo.x, y - distTo.y, z - distTo.z);
	return (float)sqrt(pow((double)d.x,2) + pow((double)d.y,2) + pow((double)d.z,2));
}

glm::vec3 ChunkCoord::Vec3() {
	return glm::vec3(x,y,z);
}

bool ChunkCoord::eq (ChunkCoord rhs) {
	return rhs.x == x && rhs.y == y && rhs.z == z;
}
