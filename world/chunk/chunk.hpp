#pragma once

#include <vector>
#include <cmath>
#include <cstdint>
#include <cstdio>

//GL functions
#include <GL/glew.h>

//GL Math
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

//Windowing
#include <GLFW/glfw3.h>

//Voxel Type
#include "../VoxelTypes.hpp"

//Noise Generator
#include "../chunksource/FastNoise/FastNoise.h"

#define TexMapSize 16 //Edge Length

/*
Since it's not practical to mesh the entire world at once,
we split it into Chunks.
Inherits from Vobject, which provides meshing and rendering
*/

class ChunkCoord {
	public:
		ChunkCoord(int64_t x, int64_t y, int64_t z);
		ChunkCoord();

		float distance(const ChunkCoord& distTo);

		bool eq(ChunkCoord rhs);

		glm::vec3 Vec3();

		int64_t x;
		int64_t y;
		int64_t z;
};

class Chunk {
	public:
		/*
		Constructor:
		Size = Size of Object
		Scale = Size of Voxel (1 = 1 voxel per GL world coord)
		*/
		Chunk(uint8_t Size, float Scale, ChunkCoord position);
		~Chunk(); //Destructor

		enum State {
			S_EMPTY,	//Empty Vobj
			S_VGENED, //Vobj Voxels Generated
			S_MESH,	 //Vobj Meshed
			S_READY,	//Vobj Meshed & Buffered
			S_NMESH,	//No voxels to mesh, don't bother meshing
			S_REMESH	//Need to remesh chunk
		};

		ChunkCoord position;

		uint8_t State; //Holds State - Whether generated or not

		//UV, VERT, NORM, TYPE
		GLuint GLbuffers[4];
		GLuint VertexArray;

		std::vector < glm::vec3 > vertices;
		std::vector < glm::vec2 > uvs;
		std::vector < glm::vec3 > normals;
		std::vector < GLubyte > types;

		uint8_t GetVoxelType(int x, int y, int z);

		//Generate Mesh, returns verticies, uvs and normals of Voxel object
		void GenCullMesh();

		//Greedy Mesher, more optimal than the Cull Mesher TODO
		//bool GenGreedyMesh();

		//Generate Voxel Data //TODO Move to another class, in order to make chunk class more general?
		void GenChunk();
		void GenFlatChunk();

		//Get model Matrix
		glm::mat4 GetModelMatrix();

		//Old things
		bool Exists;
		bool EmptyVoxels;

		//Manipulate Voxel
		void SetVoxel(int, int, int, uint8_t);

		//Generate GL Buffers
		void GenBuffers();
		void Draw();

		// Get and Set Data
		/*const uint8_t* GetVoxelData();
		void SetVoxelData(const uint8_t*);*/

		uint8_t *Voxels;
		uint16_t Size; //Side Length of Chunk (cube)
		float Scale;	//Voxel Scale

	private:
		unsigned Offset(int x, int y, int z);
		void GenSingleVoxelMesh(int x, int y, int z);
		void ClearMesh(); //Clear MEsh Data
		int Verticies;
};
