#include "world.hpp"
#define GLM_PRECISION_HIGHP_FLOAT

World::World(float _Scale, uint64_t _Size, unsigned int NumThreads, unsigned int CSize, ChunkSource* _Generator) : Chunks(_Size) {
	ChunkSize = (uint8_t)CSize;
	running = true;
	VoxelScale = _Scale;
	OctreeSize = _Size;
	Generator = _Generator;
	for(unsigned int i=0; i < NumThreads; i++) {
		GenerationThreadPool.push_back(std::thread(&World::ComputeChunk, this)); //Start worker threads
	}

	GenerationThreadPool.push_back(std::thread(&World::GenRequiredChunks, this)); //Start worker threads
}

World::~World() {
	//Stop threads
	running = false;
	for(unsigned int i=0; i<GenerationThreadPool.size();i++) {
		GenerationThreadPool[i].join();
	}

	//Delete Chunks
	for(unsigned int i=0; i< ChunkVector.size(); i++) {
		delete ChunkVector[i].ChunkPointer;
	}
}

//Can happen concurrently, does not depend on OpenGL
void World::AddChunk(ChunkCoord position) {
	Chunk* C = new Chunk(ChunkSize, VoxelScale, position); //Pointer to heap, because we don't want it moving around
	C->Exists = false;
	ChunkRef CRef;
	CRef.ChunkPointer = C;
	CRef.Position = position;

	ChunkVec.lock();
	ChunkVector.push_back(CRef); //Set Vector, used for deletion queue among other things (Maybe less efficient to check Octree??)
	ChunkVec.unlock();
	SetChunk(position, CRef);		//Set Octree

	WChunks.lock();
	WantedChunks.push(CRef); //Add to queue for worker thread/s to build
	WChunks.unlock();
}

uint8_t World::GetVoxelType(ChunkCoord Chunk, glm::vec3 VoxelCoord) {
	//Deal with chunk boundaries... again, see SetVoxel()
	ChunkCoord Transformed = Chunk;
	glm::vec3 TransformedV = VoxelCoord;

	if(VoxelCoord.x < 0) {Transformed.x -= 1; TransformedV.x = ChunkSize + VoxelCoord.x;}
	if(VoxelCoord.x > ChunkSize-1) {Transformed.x += 1; TransformedV.x = VoxelCoord.x - ChunkSize;}

	if(VoxelCoord.y < 0) {Transformed.y -= 1; TransformedV.y = ChunkSize + VoxelCoord.y;}
	if(VoxelCoord.y > ChunkSize-1) {Transformed.y += 1; TransformedV.y = VoxelCoord.y - ChunkSize;}

	if(VoxelCoord.z < 0) {Transformed.z -= 1; TransformedV.z = ChunkSize + VoxelCoord.z;}
	if(VoxelCoord.z > ChunkSize-1) {Transformed.z += 1; TransformedV.z = VoxelCoord.z - ChunkSize;}

	uint8_t type = 0;
	ChunkRef a = GetChunk(Transformed);
	if(a.ChunkPointer != NULL)
		type = a.ChunkPointer->GetVoxelType(int(TransformedV.x), int(TransformedV.y), int(TransformedV.z));
	return type;
}


//Gets surface intersection point of voxel as coord inside voxel, and transformation vector for next voxel
std::tuple<glm::vec3, glm::vec3> World::GetNextVoxelIntersection(glm::vec3 CamDir, glm::vec3 PositionInVoxel) {
	//Get smallest multiple of camDir unit vector that gets us to the edge of the voxel
	float minmul = 100.0f;
	glm::vec3 dirVec = glm::vec3(0);
	for(int i=0; i<3; i++) {
	float mul = 0;
	int8_t dir = 0;
	if( CamDir[i] > 0) { mul = (1-PositionInVoxel[i])/CamDir[i]; dir=1; } //For x,y,z == 1
	if( CamDir[i] < 0) { mul = (-PositionInVoxel[i])/CamDir[i]; dir=-1; } //For x,y,z == 0
	if(fabs(mul) < fabs(minmul)) {
		minmul = mul;
		dirVec = glm::vec3(0);
		dirVec[i] = dir;
	}
	}

	std::tuple<glm::vec3, glm::vec3> out;
	out = {glm::vec3(PositionInVoxel + CamDir*minmul), dirVec};
	return out;
}


glm::vec3 World::GetNextNonEmptyRecurse(glm::vec3 CamDir, ChunkCoord PlayerChunk, glm::vec3 PositionInVoxel, glm::vec3 VoxelCoord, int Distance, glm::vec3 PlayerVoxel, bool LastAir) {
	glm::vec3 Voxel;
	glm::vec3 OldVoxel = VoxelCoord;
	//Get intersection
	std::tuple<glm::vec3, glm::vec3> intersection = GetNextVoxelIntersection(CamDir, PositionInVoxel);

	//Figure out new voxel coords
	VoxelCoord += std::get<1>(intersection);
	PositionInVoxel = std::get<0>(intersection) - std::get<1>(intersection);
	//printf("\n%f:%f:%f", PositionInVoxel.x, PositionInVoxel.y, PositionInVoxel.z);

	//Deal with chunk boundaries
	ChunkCoord Transformed = PlayerChunk;
	glm::vec3 TransformedV = VoxelCoord;
	if(VoxelCoord.x < 0) {Transformed.x -= 1; TransformedV.x = ChunkSize + VoxelCoord.x;}
	if(VoxelCoord.x > ChunkSize-1) {Transformed.x += 1; TransformedV.x = VoxelCoord.x - ChunkSize;}

	if(VoxelCoord.y < 0) {Transformed.y -= 1; TransformedV.y = ChunkSize + VoxelCoord.y;}
	if(VoxelCoord.y > ChunkSize-1) {Transformed.y += 1; TransformedV.y = VoxelCoord.y - ChunkSize;}

	if(VoxelCoord.z < 0) {Transformed.z -= 1; TransformedV.z = ChunkSize + VoxelCoord.z;}
	if(VoxelCoord.z > ChunkSize-1) {Transformed.z += 1; TransformedV.z = VoxelCoord.z - ChunkSize;}

	ChunkRef Ch = GetChunk(Transformed);
	Voxel = VoxelCoord;
	if(Ch.ChunkPointer != NULL) {
		uint8_t VoxelType = Ch.ChunkPointer->GetVoxelType(int(TransformedV.x), int(TransformedV.y), int(TransformedV.z));

		if (VoxelType != 0 && LastAir) {
			 Voxel = OldVoxel;
		} else if (VoxelType == 0) {
			//printf(" %d", Distance);
			if (Distance > 0) {
				Voxel = GetNextNonEmptyRecurse(CamDir, PlayerChunk, PositionInVoxel, VoxelCoord, Distance-1, PlayerVoxel, LastAir);
			} else {
				Voxel = PlayerVoxel; //If we don't find a voxel within distance, set response to same as player voxel, so player can know.
			}
		}
	}


	return Voxel;
}


void World::SetVoxelIfExists(ChunkCoord position, glm::vec3 voxel, uint8_t type) {
	Chunk* Chunk = GetChunk(position).ChunkPointer;
	if(Chunk != NULL) {
		Chunk->SetVoxel(int(voxel.x), int(voxel.y), int(voxel.z), type);
		Generator->SaveChunk(GetChunk(position));
	}
}


//Deletes Voxel with coords relative to current chunk
void World::SetVoxel(ChunkCoord Chunk, glm::vec3 VoxelCoord, uint8_t type) {

	//Deal with chunk boundaries
	ChunkCoord Transformed = Chunk;
	glm::vec3 TransformedV = VoxelCoord;

	if(VoxelCoord.x < 0) {Transformed.x -= 1; TransformedV.x = ChunkSize + VoxelCoord.x;}
	if(VoxelCoord.x > ChunkSize-1) {Transformed.x += 1; TransformedV.x = VoxelCoord.x - ChunkSize;}

	if(VoxelCoord.y < 0) {Transformed.y -= 1; TransformedV.y = ChunkSize + VoxelCoord.y;}
	if(VoxelCoord.y > ChunkSize-1) {Transformed.y += 1; TransformedV.y = VoxelCoord.y - ChunkSize;}

	if(VoxelCoord.z < 0) {Transformed.z -= 1; TransformedV.z = ChunkSize + VoxelCoord.z;}
	if(VoxelCoord.z > ChunkSize-1) {Transformed.z += 1; TransformedV.z = VoxelCoord.z - ChunkSize;}

	//printf("SetChunk: x:%d y:%d z:%d t:%d\n", Transformed.x, Transformed.y, Transformed.z, type);
	//printf("SetVoxel: x:%f y:%f z:%f t:%d\n", TransformedV.x, TransformedV.y, TransformedV.z, type);


	//Delete
	SetVoxelIfExists(Transformed, TransformedV, type);

	// There has to be a more concise way...
	if(TransformedV.x < 0.1f)	{
		ChunkCoord Adjacent = Transformed;
		Adjacent.x -= 1;
		SetVoxelIfExists(Adjacent, glm::vec3(ChunkSize, TransformedV.y, TransformedV.z), type);
	}

	if(TransformedV.x > ChunkSize -1.1f)	{
		ChunkCoord Adjacent = Transformed;
		Adjacent.x += 1;
		SetVoxelIfExists(Adjacent, glm::vec3(-1, TransformedV.y, TransformedV.z), type);
	}

	if(TransformedV.y < 0.1f)	{
		ChunkCoord Adjacent = Transformed;
		Adjacent.y -= 1;
		SetVoxelIfExists(Adjacent, glm::vec3(TransformedV.x, ChunkSize, TransformedV.z), type);
	}

	if(TransformedV.y > ChunkSize -1.1f)	{
		ChunkCoord Adjacent = Transformed;
		Adjacent.y += 1;
		SetVoxelIfExists(Adjacent, glm::vec3(TransformedV.x, -1, TransformedV.z), type);
	}

	if(TransformedV.z < 0.1f)	{
		ChunkCoord Adjacent = Transformed;
		Adjacent.z -= 1;
		SetVoxelIfExists(Adjacent, glm::vec3(TransformedV.x, TransformedV.y, ChunkSize), type);
	}

	if(TransformedV.z > ChunkSize-1.1f)	{
		ChunkCoord Adjacent = Transformed;
		Adjacent.z += 1;
		SetVoxelIfExists(Adjacent, glm::vec3(TransformedV.x, TransformedV.y, -1), type);
	}
}


//This always runs in a thread
void World::ComputeChunk() {
	do {
		ChunkRef Cref;
		WChunks.lock();
		if(!WantedChunks.empty()) { //If there is stuff in the wanted queue
			Cref = WantedChunks.front();
			WantedChunks.pop();
		}
		WChunks.unlock();

		if(Cref.ChunkPointer != NULL) { //If we did actually get a chunk
			//We have our Chunk pointer, now we can do stuff to it.
			if(Cref.ChunkPointer->State == Chunk::S_EMPTY) {
				Generator->GetChunk(&Cref);	//Generate Voxel Data
			}
			Cref.ChunkPointer->GenCullMesh(); //Generate Mesh - Changes mesh flag
			Cref.ChunkPointer->Exists = true;
		} else { //No Chunks to generate
			std::this_thread::sleep_for (std::chrono::milliseconds(50)); //Sleep for 50ms if there is nothing to process, so we don't hog cycles
		}
		//Yield to other threads
		std::this_thread::yield();
	} while (running);
}


//Fast
//Cull Chunks that are too far away
void World::DelChunks(ChunkCoord PlayerChunk, float Distance) {
	for(unsigned int i=0; i < ChunkVector.size(); i++) {
		if(PlayerChunk.distance(ChunkVector[i].Position) > Distance+2) {
			if(ChunkVector[i].ChunkPointer->Exists) { //Check if Chunk currently is generated
				// printf("DelBuffer %ld:%ld:%ld E:%d\n", ChunkVector[i].Position.x, ChunkVector[i].Position.y, ChunkVector[i].Position.z, ChunkVector[i].ChunkPointer->Exists);

				Chunk* C = ChunkVector[i].ChunkPointer;
				ChunkVec.lock();
				ChunkVector[i].ChunkPointer = NULL;
				EraseChunk(ChunkVector[i].Position); //Remove from Octree
				ChunkVector.erase(ChunkVector.begin()+i); //Remove From Vector
				ChunkVec.unlock();

				//Chunk Object is still allocated, but needs to be deallocated in opengl thread
				DChunks.lock();
				ChunksToDelete.push(C); //Push chunk to GPU deletion queue
				DChunks.unlock();
				i--; //We removed an element, so size-1
			}
		}
	}
}


void World::GenChunksAroundPlayer(ChunkCoord PlayerChunk, float ChunkDistance) {
	GChunks.lock();
	GRequiredChunks.push(GenRequest(PlayerChunk, ChunkDistance));
	GChunks.unlock();
}


//This function is not slow anymore!
//Only run it when we change chunks
void World::GenRequiredChunks() {
	while(running) {
		GenRequest Req;
		GChunks.lock();
		if(!GRequiredChunks.empty()) { //If there is stuff in the wanted queue
			Req = GRequiredChunks.back();

			while (!GRequiredChunks.empty()) {
				GRequiredChunks.pop();
			}

			GChunks.unlock();
			for(int64_t x = Req.PlayerChunk.x-(int64_t)Req.ChunkDistance; x < Req.PlayerChunk.x+(int64_t)Req.ChunkDistance; x++) {
				for(int64_t y = Req.PlayerChunk.y-(int64_t)Req.ChunkDistance; y < Req.PlayerChunk.y+(int64_t)Req.ChunkDistance; y++) {
					for(int64_t z = Req.PlayerChunk.z-(int64_t)Req.ChunkDistance; z < Req.PlayerChunk.z+(int64_t)Req.ChunkDistance; z++) {
						ChunkCoord Coord = ChunkCoord(x,y,z);
						ChunkCoord Oct = OctreeSpace(Coord); //i.e. -x->x  0->2x
						if(Req.PlayerChunk.distance(Coord) < Req.ChunkDistance && ((uint64_t)Oct.x < OctreeSize && (uint64_t)Oct.y < OctreeSize && (uint64_t)Oct.z < OctreeSize)) {
								if(GetChunk(Coord).ChunkPointer == NULL) {
								AddChunk(Coord); //Add Chunk to Make Queue
							}
						}
					}
				}
			}
			//clock_t stop = clock();
			DelChunks(Req.PlayerChunk, Req.ChunkDistance);
		} else {
			GChunks.unlock();
			std::this_thread::sleep_for (std::chrono::milliseconds(50)); //Sleep for 50ms if there is nothing to process, so we don't hog cycles
		}
	}
}


//Not Multithreaded, must be called in the main thread OpenGL limitation
void World::DrawChunks(glm::mat4 m) {

	//Get clipping planes for frustum culling
	//glm::mat4 m = player->getVPMatrix();

	glm::vec4 FrustumPlane_Right;
	FrustumPlane_Right.x = m[0][3] + m[0][0];
	FrustumPlane_Right.y = m[1][3] + m[1][0];
	FrustumPlane_Right.z = m[2][3] + m[2][0];
	FrustumPlane_Right.w = m[3][3] + m[3][0];

	glm::vec4 FrustumPlane_Left;
	FrustumPlane_Left.x = m[0][3] - m[0][0];
	FrustumPlane_Left.y = m[1][3] - m[1][0];
	FrustumPlane_Left.z = m[2][3] - m[2][0];
	FrustumPlane_Left.w = m[3][3] - m[3][0];

	glm::vec4 FrustumPlane_Top;
	FrustumPlane_Top.x = m[0][3] - m[0][1];
	FrustumPlane_Top.y = m[1][3] - m[1][1];
	FrustumPlane_Top.z = m[2][3] - m[2][1];
	FrustumPlane_Top.w = m[3][3] - m[3][1];

	glm::vec4 FrustumPlane_Bottom;
	FrustumPlane_Bottom.x = m[0][3] + m[0][1];
	FrustumPlane_Bottom.y = m[1][3] + m[1][1];
	FrustumPlane_Bottom.z = m[2][3] + m[2][1];
	FrustumPlane_Bottom.w = m[3][3] + m[3][1];

	glm::vec4 FrustumPlane_Near;
	FrustumPlane_Near.x = m[0][3] - m[0][2];
	FrustumPlane_Near.y = m[1][3] - m[1][2];
	FrustumPlane_Near.z = m[2][3] - m[2][2];
	FrustumPlane_Near.w = m[3][3] - m[3][2];



	RenderedChunks = 0;
	ChunkVec.lock();
	for (unsigned int i =0; i< ChunkVector.size(); i++) {
		ChunkCoord ChunkLoc = ChunkVector[i].Position;

		//Convert chunk coord to opengl space
		glm::vec3 pt = glm::vec3((float)ChunkLoc.x*ChunkSize*VoxelScale, (float)ChunkLoc.y*ChunkSize*VoxelScale, (float)ChunkLoc.z*ChunkSize*VoxelScale);

		//Find distances to planes
		float dr = FrustumPlane_Right.x*pt.x + FrustumPlane_Right.y*pt.y + FrustumPlane_Right.z*pt.z + FrustumPlane_Right.w;
		float dl = FrustumPlane_Left.x*pt.x + FrustumPlane_Left.y*pt.y + FrustumPlane_Left.z*pt.z + FrustumPlane_Left.w;
		float dt = FrustumPlane_Top.x*pt.x + FrustumPlane_Top.y*pt.y + FrustumPlane_Top.z*pt.z + FrustumPlane_Top.w;
		float db = FrustumPlane_Bottom.x*pt.x + FrustumPlane_Bottom.y*pt.y + FrustumPlane_Bottom.z*pt.z + FrustumPlane_Bottom.w;
		float dn = FrustumPlane_Near.x*pt.x + FrustumPlane_Near.y*pt.y + FrustumPlane_Near.z*pt.z + FrustumPlane_Near.w;

		float compare = ChunkSize*-1.4142135623730951f; //sqrt(2)
		if(dr > compare && dl > compare && dt > compare && db > compare && dn > compare) {
			//if(true) {
			ChunkVector[i].ChunkPointer->Draw();
			RenderedChunks++;
		}
	}
	ChunkVec.unlock();

	//Delete chunks in opengl thread
	DChunks.lock();
	while(!ChunksToDelete.empty()) { //If there is stuff in the wanted queue
	Chunk* C = ChunksToDelete.front();
	ChunksToDelete.pop();
	delete C;
	}
	DChunks.unlock();

	//printf("Queue: %d\n" ,WantedChunks.size());
}

inline ChunkCoord World::OctreeSpace(ChunkCoord WorldSpace) {
	uint64_t Sz = OctreeSize/2;
	ChunkCoord OctSpace = ChunkCoord(WorldSpace.x + Sz, WorldSpace.y + Sz, WorldSpace.z + Sz);
	//printf("Get: %u:%u:%u\n", OctSpace.x,OctSpace.y,OctSpace.z);
	return OctSpace;
}

//Not ThreadSafe, unless use external locks
ChunkRef World::GetChunk(ChunkCoord w) {
	ChunkCoord p = OctreeSpace(w);
	if((uint64_t)p.x > OctreeSize || (uint64_t)p.y > OctreeSize || (uint64_t)p.z > OctreeSize) {
		printf("Too Big!\n");
		printf("Get: %lu:%lu:%lu\n", p.x,p.y,p.z);
	}

	return Chunks.Get(p.x,p.y,p.z);
}

void World::SetChunk(ChunkCoord w, ChunkRef Chunk){
	ChunkCoord p = OctreeSpace(w);
	if((uint64_t)p.x > OctreeSize || (uint64_t)p.y > OctreeSize || (uint64_t)p.z > OctreeSize) {
		printf("Too Big!\n");
		printf("Get: %lu:%lu:%lu\n", p.x,p.y,p.z);
	}
	//printf("Set: %d:%d:%d\n", p.x,p.y,p.z);
	ChunkOctree.lock();
	Chunks.Set(p.x,p.y,p.z, Chunk);
	ChunkOctree.unlock();
}

void World::EraseChunk(ChunkCoord w) {
	ChunkCoord p = OctreeSpace(w);
	if((uint64_t)p.x > OctreeSize || (uint64_t)p.y > OctreeSize || (uint64_t)p.z > OctreeSize) {
		printf("Too Big!\n");
		printf("Get: %lu:%lu:%lu\n", p.x,p.y,p.z);
	}
	ChunkOctree.lock();
	Chunks.Erase(p.x,p.y,p.z);
	ChunkOctree.unlock();
}

uint64_t World::GetNChunks() {
	return ChunkVector.size();
}

uint64_t World::GetWaitingChunks() {
	return WantedChunks.size();
}

GenRequest::GenRequest(ChunkCoord _PlayerChunk ,float _ChunkDistance) {
	PlayerChunk = _PlayerChunk;
	ChunkDistance = _ChunkDistance;
}

GenRequest::GenRequest() {}
