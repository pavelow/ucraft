#pragma once

enum VoxelTypes {
  V_EMPTY,
  V_GRASS,
  V_GRASSDIRT,
  V_DIRT,
  V_STONE,
  V_SAND
};

char* VoxelString(uint8_t type);
