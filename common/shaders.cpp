#include "shaders.hpp"


GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path, const char * geometry_file_path) {
	// Create the shaders
	printf("Loading shaders: vert:%s frag:%s geo:%s\n", vertex_file_path, fragment_file_path, geometry_file_path);
	std::vector<GLuint> Shaders;
	Shaders.push_back(glCreateShader(GL_VERTEX_SHADER));
	Shaders.push_back(glCreateShader(GL_GEOMETRY_SHADER));
	Shaders.push_back(glCreateShader(GL_FRAGMENT_SHADER));

	// Read Shaders/Compile them
	CompileShader(vertex_file_path, Shaders[0]);
	CompileShader(geometry_file_path, Shaders[1]);
	CompileShader(fragment_file_path, Shaders[2]);

	//Link
	GLuint ProgramID = LinkProgram(Shaders);

	return ProgramID;
}

GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path) {
	// Create the shaders
	printf("Loading shaders: vert:%s frag:%s\n", vertex_file_path, fragment_file_path);
	std::vector<GLuint> Shaders;
	Shaders.push_back(glCreateShader(GL_VERTEX_SHADER));
	Shaders.push_back(glCreateShader(GL_FRAGMENT_SHADER));

	// Read Shaders/Compile them
	CompileShader(vertex_file_path, Shaders[0]);
	CompileShader(fragment_file_path, Shaders[1]);

	//Link
	GLuint ProgramID = LinkProgram(Shaders);

	return ProgramID;
}



std::string LoadFile(const char * file_path) {
	// Read the Vertex Shader code from the file
	std::string ShaderCode;
	std::ifstream ShaderStream(file_path, std::ios::in);
	if(ShaderStream.is_open()){
		std::stringstream sstr;
		sstr << ShaderStream.rdbuf();
		ShaderCode = sstr.str();
		ShaderStream.close();
	}else{
		printf("Couldn't open %s, does it exist?\n", file_path);
		getchar();
		return 0;
	}
	return ShaderCode;
}

void CompileShader(const char * file_path, GLuint ShaderID) {
	std::string ShaderCode = LoadFile(file_path);
	// Compile Shader
	char const * SourcePointer = ShaderCode.c_str();
	glShaderSource(ShaderID, 1, &SourcePointer , NULL);
	glCompileShader(ShaderID);

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Check Shader
	glGetShaderiv(ShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(ShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> ShaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(ShaderID, InfoLogLength, NULL, &ShaderErrorMessage[0]);
		printf("%s\n", &ShaderErrorMessage[0]);
	}
}

GLuint LinkProgram(std::vector<GLuint> Shaders) {
	// Link the program
	GLuint ProgramID = glCreateProgram();
	for(uint64_t i=0; i < Shaders.size(); i++) {
		glAttachShader(ProgramID, Shaders[i]);
	}

	glLinkProgram(ProgramID);

	GLint Result = GL_FALSE;
	int InfoLogLength;
	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> ProgramErrorMessage(InfoLogLength+1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
	}

	for(uint64_t i=0; i < Shaders.size(); i++) {
		glDetachShader(ProgramID, Shaders[i]);
		glDeleteShader(Shaders[i]);
	}

	return ProgramID;
}
