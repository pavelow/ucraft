#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <GL/glew.h>
GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path);
GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path, const char * geometry_file_path);
std::string LoadFile(const char * file_path);

void CompileShader(const char * file_path, GLuint ShaderID);
GLuint LinkProgram(std::vector<GLuint> Shaders);
