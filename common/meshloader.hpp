#include <glm/glm.hpp>
#include <vector>
#include <stdio.h>
#include <cstring>
#include <string>

bool loadOBJ(
	const char * path,
	std::vector < glm::vec3 > & out_vertices,
	std::vector < glm::vec2 > & out_uvs,
	std::vector < glm::vec3 > & out_normals
);
