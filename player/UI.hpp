#pragma once

//#include "../common/textOverlay.hpp" //Text Overlay
#include "../common/shaders.hpp" //Text Overlay

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <cstdio>
#include <cstdlib>

class UIOverlay {
	public:
		UIOverlay(); //Constructor
		~UIOverlay();

		void SetMode();
		void DrawCube(glm::mat4 mvp);
		void DrawReticle();

	private:
		//Cube Verticies
		enum CubeCoords {FLB, FRB, FLT, FRT, BLB, BRB, BLT, BRT};
		glm::vec3 CubeVerts[8] = {
			glm::vec3(-1.0f,-1.0f, 1.0f),
			glm::vec3(1.0f,-1.0f, 1.0f),
			glm::vec3(-1.0f,1.0f, 1.0f),
			glm::vec3(1.0f,1.0f, 1.0f),
			glm::vec3(-1.0f,-1.0f, -1.0f),
			glm::vec3(1.0f,-1.0f, -1.0f),
			glm::vec3(-1.0f,1.0f, -1.0f),
			glm::vec3(1.0f,1.0f, -1.0f)
		};

		std::vector < glm::vec3 > vertices;

		GLuint Wire;
		GLuint CubeEdges;
		GLuint CubeV;
		GLuint MatrixID;
		size_t VertLength;

		GLuint Reticle;
		GLuint ReticleV;
		int ReticleVertLength;


};
