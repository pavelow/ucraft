#include "player.hpp"
#include <cstdio>
#include <cmath>

#ifndef M_PI
#define M_PI 3.14159265359 //MSVC Needs this :(
#endif

Player::Player(World* _world, glm::vec3 StartPos, int &rX, int &rY, float _ChunkDistance) {
	resX = &rX;
	resY = &rY;
	world = _world;
	ChunkSize = world->ChunkSize;
	VoxelScale = world->VoxelScale;
	position = StartPos;
	fly = true;
	ChunkDistance = _ChunkDistance;
	PrevPlayerChunk = ChunkCoord(0,1,0); //This has to not be the starting chunk

	SelectedVoxel = V_STONE;

	UI = UIOverlay();
}

void Player::computeMatricesFromInputs(GLFWwindow* window) {
	static bool flyPress = false;
	static double lastTime = 0;
	double currentTime = glfwGetTime();
	float deltaTime = float(currentTime - lastTime);
	lastTime = currentTime;

	//Get Mouse Position
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);		 //Get mouse position	

	//Set angles based on current mouse position
	horizontalAngle = mouseSpeed * -xpos;

	//Normalise horizontal angle	0 < x < 2pi
	if (horizontalAngle < 0) {horizontalAngle = 2*(float)M_PI + horizontalAngle;}
	if (horizontalAngle > 2*(float)M_PI) {horizontalAngle = horizontalAngle - 2*(float)M_PI;}

	verticalAngle = mouseSpeed * -ypos;
	//Clamp Vertical angle -pi < x < pi
	//(Don't want camera to rotate over the top and end up upside down, using lower precision pi due to wrap around from floating point error)
	if(verticalAngle > 3.14f/2) {verticalAngle = 3.14f/2;}
	if(verticalAngle < -3.14f/2) {verticalAngle = -3.14f/2;}

	CameraDirection = glm::vec3(
		cos(verticalAngle) * sin(horizontalAngle),
		sin(verticalAngle),
		cos(verticalAngle) * cos(horizontalAngle)
	);

	PlayerDirection = glm::vec3(
		cos(0) * sin(horizontalAngle),
		sin(0),
		cos(0) * cos(horizontalAngle)
	);

	glm::vec3 right = glm::vec3(
		sin(horizontalAngle - M_PI/2.0f),
		0,
		cos(horizontalAngle - M_PI/2.0f)
	);

	//up = cross(right, direction);
	// Move forward
	if (glfwGetKey(window, GLFW_KEY_W ) == GLFW_PRESS){
		acceleration += PlayerDirection * force;
	}
	// Move backward
	if (glfwGetKey(window, GLFW_KEY_S ) == GLFW_PRESS){
		acceleration -= PlayerDirection * force;
	}
	// Strafe right
	if (glfwGetKey(window, GLFW_KEY_D ) == GLFW_PRESS){
		acceleration += right * force;
	}
	// Strafe left
	if (glfwGetKey(window, GLFW_KEY_A ) == GLFW_PRESS){
		acceleration -= right * force;
	}

	if (glfwGetKey(window, GLFW_KEY_SPACE ) == GLFW_PRESS && fly){
		acceleration += up * force;
	}

	if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL ) == GLFW_PRESS && fly){
		acceleration -= up * force;
	}

	int state = glfwGetKey(window, GLFW_KEY_F );
	if (state == GLFW_PRESS && !flyPress){
		fly = !fly;
		flyPress = true;
	} else if(state == GLFW_RELEASE) {
		flyPress = false;
	}


	velocity = velocity * (1-(friction*deltaTime)); //* deltaTime; //Deceleration
	velocity += acceleration;

	//Gravity
	if(!fly) {
		acceleration = glm::vec3(0,-9.8,0);
	} else {
		acceleration = glm::vec3(0,0,0);
	}

	position += velocity * deltaTime;
	if(length(velocity) > maxVel) {
		velocity = normalize(velocity) * maxVel;
	}

	//printf("%f:%f:%f\n", velocity.x, velocity.y, velocity.z );

}
glm::mat4 Player::getProjectionMatrix() {
	return glm::perspective(glm::radians(initialFoV), float(*resX)/float(*resY), 0.1f, ChunkDistance*32); //Tells us how verticies are projected to screen space
}

glm::mat4 Player::getViewMatrix() {
	return lookAt(position, position+CameraDirection, up); //Tells us position and rotation of camera
}

glm::vec3 Player::getCameraNormal() {
	return CameraDirection;
}

glm::vec2 Player::getCameraDir() {
	return glm::vec2(horizontalAngle, verticalAngle);
}

glm::vec3 Player::getCameraDirU() {
	return CameraDirection;
}

glm::mat4 Player::getVPMatrix() {
	return Player::getProjectionMatrix() * Player::getViewMatrix();
}

//Calculates Chunk that the player is in
void Player::CalculatePlayerChunk() {
	glm::vec3 playerpos = position/(ChunkSize*VoxelScale); //16.0f
	glm::vec3 currentChunkVec = glm::vec3(floor(playerpos+glm::vec3(0.5f, 0.0f, 0.5f)));


	PlayerVoxelSub = (((playerpos-currentChunkVec+glm::vec3(0.5f, 0.0f, 0.5f))*float(world->ChunkSize))+glm::vec3(0.5f, 0.5f, 0.5f));

	//Is there is a better solution? I can't see it a 2:59AM (w/o this voxel coords go out of range)
	//	(╯°□°）╯︵ ┻━┻
	if(PlayerVoxelSub.x >= ChunkSize) {PlayerVoxelSub.x -= ChunkSize; currentChunkVec.x += 1.0f;}
	if(PlayerVoxelSub.y >= ChunkSize) {PlayerVoxelSub.y -= ChunkSize; currentChunkVec.y += 1.0f;}
	if(PlayerVoxelSub.z >= ChunkSize) {PlayerVoxelSub.z -= ChunkSize; currentChunkVec.z += 1.0f;}

	PlayerVoxel = floor(PlayerVoxelSub);
	PlayerVoxelSub = PlayerVoxelSub-PlayerVoxel;
	//PlayerVoxelSub = (((playerpos-currentChunkVec+glm::vec3(0.5f, 0.0f, 0.5f))*float(ChunkSize))-PlayerVoxel)+glm::vec3(0.5f, 0.5f, 0.5f);

	// TODO: find a way to use a uint64_t currentChunkVec rather than a float
	PlayerChunk = ChunkCoord((int64_t)currentChunkVec.x, (int64_t)currentChunkVec.y, (int64_t)currentChunkVec.z);

	PlayerLookAtVoxel = world->GetNextNonEmptyRecurse(getCameraDirU(), PlayerChunk, PlayerVoxelSub, PlayerVoxel, SelectRadius, PlayerVoxel, false);
	PlayerLookAtSurfaceVoxel = world->GetNextNonEmptyRecurse(getCameraDirU(), PlayerChunk, PlayerVoxelSub, PlayerVoxel, SelectRadius, PlayerVoxel, true);

	VoxelType = world->GetVoxelType(PlayerChunk, PlayerVoxel);

	//Add player chunk to gen queue
	if(!PrevPlayerChunk.eq(PlayerChunk)) {
		GenChunks(); // TODO Move this elsewhere
		PrevPlayerChunk = PlayerChunk;
	}


	//void world->DelChunks(ChunkCoord Chunk, float Distance);
}

void Player::GenChunks() {
	world->GenChunksAroundPlayer(PlayerChunk, ChunkDistance);
}

void Player::Draw() {
	//Draw Chunks
	world->DrawChunks(getVPMatrix());
	//Figure out Voxel GL coord
	if(PlayerLookAtVoxel != PlayerVoxel) {
		glm::vec3 pt = glm::vec3((float)PlayerChunk.x*ChunkSize*VoxelScale, (float)PlayerChunk.y*ChunkSize*VoxelScale, (float)PlayerChunk.z*ChunkSize*VoxelScale);
		pt += PlayerLookAtVoxel*VoxelScale - glm::vec3(VoxelScale*(ChunkSize/2.0f), 0, VoxelScale*(ChunkSize/2.0f));
		glm::mat4 translate = glm::translate(glm::mat4(1.0f), pt);
		glm::mat4 scale = glm::scale(glm::mat4(1.0f), glm::vec3(VoxelScale*0.51f));

		UI.DrawCube(getVPMatrix()*translate*scale);
	}
	UI.DrawReticle();
}

//Destroy block
void Player::LMBTrigger() {
	if(PlayerLookAtVoxel != PlayerVoxel) {
		world->SetVoxel(PlayerChunk, PlayerLookAtVoxel, V_EMPTY); //Set Voxel to 0
	}
}

//Create block
void Player::RMBTrigger() {
	if(PlayerLookAtSurfaceVoxel != PlayerVoxel) {
		world->SetVoxel(PlayerChunk, PlayerLookAtSurfaceVoxel, SelectedVoxel); //Set Voxel to 1
	}
}

void Player::MMBTrigger() {
	SelectedVoxel	= world->GetVoxelType(PlayerChunk, PlayerLookAtVoxel);
}

void Player::ScrollTrigger(double offset) {
	SelectedVoxel += offset;
}
