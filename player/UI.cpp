#include "UI.hpp"


UIOverlay::UIOverlay() {
	Wire = LoadShaders( "shaders/vertexWire.glsl", "shaders/fragmentWire.glsl" );
	MatrixID = glGetUniformLocation(Wire, "MVP");

	//Construct Vertex Buffer

	//Front
	vertices.push_back(CubeVerts[FLB]);
	vertices.push_back(CubeVerts[FRB]);

	vertices.push_back(CubeVerts[FLT]);
	vertices.push_back(CubeVerts[FRT]);

	vertices.push_back(CubeVerts[FLT]);
	vertices.push_back(CubeVerts[FLB]);

	vertices.push_back(CubeVerts[FRT]);
	vertices.push_back(CubeVerts[FRB]);

	//Extrude
	vertices.push_back(CubeVerts[FRT]);
	vertices.push_back(CubeVerts[BRT]);

	vertices.push_back(CubeVerts[FLT]);
	vertices.push_back(CubeVerts[BLT]);

	vertices.push_back(CubeVerts[FRB]);
	vertices.push_back(CubeVerts[BRB]);

	vertices.push_back(CubeVerts[FLB]);
	vertices.push_back(CubeVerts[BLB]);

	//Back
	vertices.push_back(CubeVerts[BLB]);
	vertices.push_back(CubeVerts[BRB]);

	vertices.push_back(CubeVerts[BLT]);
	vertices.push_back(CubeVerts[BRT]);

	vertices.push_back(CubeVerts[BLT]);
	vertices.push_back(CubeVerts[BLB]);

	vertices.push_back(CubeVerts[BRT]);
	vertices.push_back(CubeVerts[BRB]);

	//Have vertex Buffer
	VertLength = vertices.size();



	glGenVertexArrays(1, &CubeEdges);
	glBindVertexArray(CubeEdges);

	glGenBuffers(1, &CubeV);

	//Vertices Buffer
	glBindBuffer(GL_ARRAY_BUFFER, CubeV);
	glEnableVertexAttribArray(0);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);


	vertices.clear();
	vertices.push_back(CubeVerts[FLB]);
	vertices.push_back(CubeVerts[FRT]);

	vertices.push_back(CubeVerts[FLT]);
	vertices.push_back(CubeVerts[FRB]);


	glGenVertexArrays(1, &Reticle);
	glBindVertexArray(Reticle);

	glGenBuffers(1, &ReticleV);

	//Vertices Buffer
	glBindBuffer(GL_ARRAY_BUFFER, ReticleV);
	glEnableVertexAttribArray(0);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

}

UIOverlay::~UIOverlay() {

}

void UIOverlay::DrawReticle() {
	glUseProgram(Wire); //Wireframe
	glm::mat4 trans = glm::translate(glm::mat4(1.0f), glm::vec3(0,0,-1));
	trans *= glm::scale(glm::mat4(1.0f), glm::vec3(0.01f));
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &trans[0][0]); //Uniform for wireframe
	glBindVertexArray(Reticle);
	glDrawArrays(GL_LINES, 0, (int)VertLength);

}

void UIOverlay::DrawCube(glm::mat4 mvp) {

	glBindVertexArray(CubeEdges);

	//Disable things
	//glDisable(GL_CULL_FACE);
	glUseProgram(Wire); //Wireframe
	//glDisable(GL_DEPTH_TEST);

	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mvp[0][0]); //Uniform for wireframe

	//glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );



	glDrawArrays(GL_LINES, 0, (int)VertLength);

	//Undo
	//glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );/**/
	//glEnable(GL_CULL_FACE);
}
