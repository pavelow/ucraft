#pragma once

#include "../world/world.hpp"
#include "UI.hpp"
//#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
//#define GLM_PRECISION_HIGHP_FLOAT

//Deals with player transforms
class Player {
	private:
		//Player coords
		glm::vec3 CameraDirection = glm::vec3(0,0,0); //Camera Facing
		glm::vec3 PlayerDirection = glm::vec3(0,0,0); //Character Facing
		glm::vec3 up = glm::vec3(0,1,0);							//Up Vector, Y is up

		float horizontalAngle = 3.14f; // horizontal angle : toward -Z
		float verticalAngle = 0.1f; // vertical angle : 0, look at the horizon
		float initialFoV = 75.0f; // Initial Field of View

		glm::vec3 acceleration = glm::vec3(0,0,0); // units / second
		glm::vec3 velocity = glm::vec3(0,0,0); // units / second
		float maxVel = 50.0f; // units/second
		float friction = 5.0f; //How fast to slow down
		float force = 1.0f; //How fast to speed up
		float mouseSpeed = 0.001f;

		bool fly; //Am I flying?

		//Screen Resolution (For calculating aspect ratio and mouse position)
		int* resY;
		int* resX;

		//World calculations

		World* world;
		UIOverlay UI;

	public:
		Player(World* _world, glm::vec3 StartPos, int &rX, int &rY, float _ChunkDistance); //Constructor

		void CalculatePlayerChunk(); //Done		//calculate what chunk the player is in

		//World coords
		glm::vec3 PlayerVoxelSub;
		glm::vec3 PlayerVoxel;
		ChunkCoord PlayerChunk;
		ChunkCoord PrevPlayerChunk;
		glm::vec3 PlayerLookAtVoxel;
		glm::vec3 PlayerLookAtSurfaceVoxel; //Voxel at next surface
		float VoxelScale;
		uint8_t ChunkSize;
		uint8_t VoxelType;
		uint8_t SelectedVoxel;


		//Move some of these to private
		glm::vec3 position;
		float ChunkDistance;
		int SelectRadius = 8;
		glm::vec4 ViewMatrix;
		glm::vec4 ProjectionMatrix;


		void computeMatricesFromInputs(GLFWwindow* window);
		glm::mat4 getViewMatrix();
		glm::mat4 getProjectionMatrix();
		glm::mat4 getVPMatrix(); //View Projection
		glm::vec3 getCameraNormal();
		glm::vec2 getCameraDir();
		glm::vec3 getCameraDirU();

		void GenChunks();
		void Draw();


		//Controls
		void LMBTrigger();
		void RMBTrigger();
		void MMBTrigger();
		void ScrollTrigger(double offset);
};
