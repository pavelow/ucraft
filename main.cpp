//Standard libs
#include <cstdio>
#include <cstdlib>
#include <vector>

//OpenGL
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

//ImGui
#include "dependencies/imgui/imgui.h"
#include "dependencies/imgui/backends/imgui_impl_opengl3.h"
#include "dependencies/imgui/backends/imgui_impl_glfw.h"

//Loaders et al
#include "common/shaders.hpp"					//Shader Loader
#include "common/texloader.hpp"					//BMP texture Loader
#include "player/player.hpp"					//FPS Controls/Camera & MVP Calculations
#include "common/meshloader.hpp"				//OBJ Loader
#include <tomlplusplus/toml.hpp>				//Toml Parser

//Chunk Streaming
#include "world/world.hpp"						//World manager
#include "world/chunksource/chunksources.hpp"	//Chunk sources
#include "world/VoxelTypes.hpp" 				//Voxel Types


/* System State */
enum state {
	STATE_INIT,
	STATE_LOADING,
	STATE_RUNNING,
	STATE_PAUSED
};

typedef toml::parse_result Config;

struct SystemData {
	state State = STATE_LOADING;
	GLFWwindow* window;

	// Engine Handles
	ChunkSource *G;
	World* W;
	Player* P;

	Config mConfig;

	//GL stuff
	GLuint Texture;
	GLuint programID;
	GLuint WireFrame;

	GLuint CameraNormal;
	GLuint MatrixID;
	GLuint MatrixID2;
	GLuint TextureSampID;

	float percent;
};

int SwapInterval = 1;
int rX = 800;
int rY = 600;
bool clickflag = false;
bool debugkeyflag = false;
uint8_t debugflag = 0;

void window_size_callback(GLFWwindow* window, int width, int height) {
	rX = width;	//Set coords for internal
	rY = height;
	glViewport(0, 0, width, height); //Change viewport
	//Overlay.UpdateProjection(rX, rY); //Update Projection Matrix for text
	printf("Change Resolution w:%d h:%d\n", width, height);
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
	Player* player = (Player*)glfwGetWindowUserPointer(window);

	switch(button) {
		case GLFW_MOUSE_BUTTON_LEFT:
			if(action == GLFW_PRESS)
				player->LMBTrigger();
			break;
		case GLFW_MOUSE_BUTTON_RIGHT:
			if(action == GLFW_PRESS)
				player->RMBTrigger();
			break;
		case GLFW_MOUSE_BUTTON_MIDDLE:
			if(action == GLFW_PRESS)
				player->MMBTrigger();
			break;
	}
}

void mouse_scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
	Player* player = (Player*)glfwGetWindowUserPointer(window);
	player->ScrollTrigger(yoffset);
}

bool Create_Window(GLFWwindow*& window, Config C) { //Pointer to a pointer
	glewExperimental = true;

	if( !glfwInit() ) {
		fprintf( stderr, "Failed to init GLFW\n");
		return false;
	}

	glfwWindowHint(GLFW_SAMPLES, C["graphics"]["antialiasing"].value_or(0)); //4x AA
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // We don't want the old OpenGL
	glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
	glfwWindowHint(GLFW_FLOATING, GLFW_TRUE);
	glfwWindowHint(GLFW_DECORATED, GLFW_TRUE);



	if(C["graphics"]["fullscreen"].value_or(0)) {
		window = glfwCreateWindow(rX, rY, "Render", glfwGetPrimaryMonitor(), NULL);
	} else {
		window = glfwCreateWindow(rX, rY, "Render", NULL, NULL);
	}

	glfwSetWindowSizeCallback(window, window_size_callback);

	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an old Intel GPU, they are not 3.3 compatible.\n" );
		glfwTerminate();
		return false;
	}

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); //Disable cursor rendering
	glfwMakeContextCurrent(window); // Initialise GL context
	glfwSwapInterval(SwapInterval); //Vsync every 'n' screen refreshes - If I don't enable this... it runs at several thousand FPS

	return true;
}

void Loading(struct SystemData* Handles) {
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (Handles->W->GetWaitingChunks() == 0 && Handles->W->GetNChunks() > 0) {
		Handles->State = STATE_RUNNING;
	}

	Handles->percent = 0.0f;
	if (Handles->W->GetNChunks() > 0) {
		Handles->percent = 100.0f*(1.0f-(float)Handles->W->GetWaitingChunks()/(float)Handles->W->GetNChunks());
	}

	// Start the Dear ImGui frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();

	ImGuiWindowFlags window_flags = 0;
	// window_flags |= ImGuiWindowFlags_NoBackground;
	window_flags |= ImGuiWindowFlags_NoTitleBar;
	window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

	ImGui::SetNextWindowBgAlpha(0.8f);
	int width, height;
	glfwGetWindowSize(Handles->window, &width, &height);
	ImGui::SetNextWindowPos(ImVec2((float)width/2, (float)height/2), ImGuiCond_Always, ImVec2(0.5f,0.5f));

	bool open_ptr = true;

	ImGui::Begin("Loading", &open_ptr, window_flags);
	// ImGui::Text("Waiting/Generated: %lu/%lu\n", Handles->W->GetWaitingChunks(), Handles->W->GetNChunks());
	ImGui::Text("Loading %c", "|/-\\"[(int)(ImGui::GetTime() / 0.05f) & 3]);
	ImGui::SliderFloat("Loading", &Handles->percent, 0.0f, 100.0f);

	ImGui::End();

	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void Running(struct SystemData* Handles) {
	//Debug
	if(glfwGetKey(Handles->window, GLFW_KEY_F1 ) == GLFW_PRESS && !debugkeyflag) {
		debugflag++;
		if (debugflag == 4) {
			debugflag = 0;
		}
		debugkeyflag = true;
	}

	if(glfwGetKey(Handles->window, GLFW_KEY_F1 ) == GLFW_RELEASE) {
			debugkeyflag = false;
	}

	// Compute the MVP matrix from keyboard and mouse input
	Handles->P->computeMatricesFromInputs(Handles->window);
	glm::mat4 ModelMatrix = glm::mat4(1.0f);
	glm::mat4 mvp = Handles->P->getVPMatrix() * ModelMatrix;

	//Clear colour and depth buffers
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Draw Fill
	if(debugflag < 3) {
		glUseProgram(Handles->programID);
		glUniformMatrix4fv(Handles->MatrixID, 1, GL_FALSE, &mvp[0][0]);	//Uniform for Shaded
		glUniform3f(Handles->CameraNormal, Handles->P->getCameraNormal().x, Handles->P->getCameraNormal().y, Handles->P->getCameraNormal().z);	//Horrible
		Handles->P->Draw();
	}


	//Draw Wireframe
	if(debugflag > 1) {
		glUseProgram(Handles->programID);
		glDisable(GL_CULL_FACE);
		glUseProgram(Handles->WireFrame);	//Wireframe
		glUniformMatrix4fv(Handles->MatrixID2, 1, GL_FALSE, &mvp[0][0]);	//Uniform for wireframe
		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
		Handles->P->Draw();
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
		glEnable(GL_CULL_FACE);
	}


	if (debugflag) {
		ChunkCoord CPos = Handles->P->PlayerChunk;
		glm::vec3 VPos = Handles->P->PlayerVoxel;
		glm::vec3 SVPos = Handles->P->PlayerVoxelSub;
		int VType= Handles->P->VoxelType;
		glm::vec2 VDir = Handles->P->getCameraDir();
		glm::vec3 CDir = Handles->P->getCameraDirU();
		glm::vec3 FDir = Handles->P->PlayerLookAtVoxel;
		glm::vec3 SDir = Handles->P->PlayerLookAtSurfaceVoxel;

		// Start the Dear ImGui frame
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		bool open_ptr = true;

		ImGuiWindowFlags window_flags = 0;
		// window_flags |= ImGuiWindowFlags_NoBackground;
		window_flags |= ImGuiWindowFlags_NoTitleBar;
		window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

		ImGui::SetNextWindowBgAlpha(0.8f);
		ImGui::SetNextWindowPos(ImVec2(10.0f, 10.0f));

		ImGui::Begin("Statistics", &open_ptr, window_flags);
		ImGui::Text("%.3fms (%.1f fps)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		ImGui::Text("Waiting/Generated: %lu/%lu\nRendered/Generated: %d/%lu", Handles->W->GetWaitingChunks(), Handles->W->GetNChunks(), Handles->W->RenderedChunks, Handles->W->GetNChunks());
		ImGui::End();

		ImGui::SetNextWindowPos(ImVec2(10.0f, 75.0f));
		ImGui::SetNextWindowBgAlpha(0.8f);

		ImGui::Begin("Coordinates", &open_ptr, window_flags);
		ImGui::Text("Global Coord: x:%f y:%f z:%f", Handles->P->position.x, Handles->P->position.y, Handles->P->position.z);
		ImGui::Text("Chunk Coord: x:%ld y:%ld z:%ld", CPos.x, CPos.y, CPos.z);
		ImGui::Text("Voxel Coord: x:%f y:%f z:%f", VPos.x, VPos.y, VPos.z);
		ImGui::Text("Position in Voxel : x:%f y:%f z:%f", SVPos.x, SVPos.y, SVPos.z);
		ImGui::Text("VoxelType at Player: %s", VoxelString(VType));
		ImGui::Text("Looking Angles: x:%f y:%f", (VDir.x/M_PI)*180.0f, (VDir.y/M_PI)*180.0f);
		ImGui::Text("Looking Vector: x:%f y:%f z:%f", CDir.x, CDir.y, CDir.z);

		ImGui::Text("SelectedVoxel of Player: %s (%u)", VoxelString(Handles->P->SelectedVoxel), Handles->P->SelectedVoxel);

		//Voxel face direction
		if(FDir == VPos) {
			ImGui::Text("FDir: None");
		} else {
			ImGui::Text("FDir: x:%f y:%f z:%f", FDir.x, FDir.y, FDir.z);
		}

		//Voxel face direction
		if(SDir == VPos) {
			ImGui::Text("SDir: None");
		} else {
			ImGui::Text("SDir: x:%f y:%f z:%f", SDir.x, SDir.y, SDir.z);
		}

		ImGui::End();

		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	}


	// // Swap buffers
	// glfwSwapBuffers(Handles->window);
	// glfwPollEvents();
	Handles->P->CalculatePlayerChunk();
}

int main() {
	struct SystemData Handles;

	//Load Config
	Handles.mConfig = toml::parse_file("config.toml");
	
	rX = Handles.mConfig["graphics"]["width"].value_or(800);
	rY = Handles.mConfig["graphics"]["height"].value_or(600);
	SwapInterval = Handles.mConfig["graphics"]["vsync"].value_or(1);

	//Create Window
	if (!Create_Window(Handles.window, Handles.mConfig)) {
		printf("Failed to create window\n");
		return -1;
	}

	//glewExperimental=true; // Needed in core profile, allow extensions to be loaded when they are not explicitly supported
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	//OpenGL setup
	glEnable(GL_DEPTH_TEST);	//Enable Z-buffering
	glDepthFunc(GL_LESS);		//Z-buffer passes for closer things
	glEnable(GL_CULL_FACE);		//Back face culling

	// Enable blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	Handles.Texture = loadBMP("mapA.bmp");

	Handles.programID = LoadShaders( "shaders/vertex.glsl", "shaders/fragment.glsl" );
	Handles.WireFrame = LoadShaders( "shaders/vertexWire.glsl", "shaders/fragmentWire.glsl" );

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(Handles.window, GLFW_STICKY_KEYS, GL_TRUE);

	Handles.CameraNormal = glGetUniformLocation(Handles.programID, "CameraNormal");
	Handles.MatrixID = glGetUniformLocation(Handles.programID, "MVP");
	Handles.MatrixID2 = glGetUniformLocation(Handles.WireFrame, "MVP");
	Handles.TextureSampID = glGetUniformLocation(Handles.programID, "TextureSampler");

	//Set initial cursor position
	glfwSetCursorPos(Handles.window, rX/2, rY/2);

	ImGui::CreateContext();
	ImGui::StyleColorsDark();
	ImGui_ImplGlfw_InitForOpenGL(Handles.window, true);
	ImGui_ImplOpenGL3_Init("#version 330 core");


	// Initialise ChunkSource

	//ChunkSource *Gen = new gen_flat(cfg.c["voxel"]["scale"].f, cfg.c["voxel"]["chunksize"].i);
	ChunkSource *Generator = new gen_noise(Handles.mConfig["voxel"]["scale"].value_or(1.0f), Handles.mConfig["voxel"]["chunksize"].value_or(16));

	ChunkSource *Persistance = new ucFile("chunks.uc", Handles.mConfig["voxel"]["octree"].value_or(256), Handles.mConfig["voxel"]["chunksize"].value_or(16), Handles.mConfig["voxel"]["scale"].value_or(1.0f), 256);

	if (!Persistance->Ready()) {
		printf("File already exists, opening\n");
		delete Persistance;
		Persistance = new ucFile("chunks.uc");
	}

	Handles.G = new gen_persist(Handles.mConfig["voxel"]["scale"].value_or(1.0f), Handles.mConfig["voxel"]["chunksize"].value_or(16), Generator, Persistance);
	Handles.W = new World(Handles.mConfig["voxel"]["scale"].value_or(1.0f), Handles.mConfig["voxel"]["octree"].value_or(256), Handles.mConfig["voxel"]["threads"].value_or(1), Handles.mConfig["voxel"]["chunksize"].value_or(16), Handles.G);
	Handles.P = new Player(Handles.W, glm::vec3(0,5,0), rX, rY, (float)Handles.mConfig["voxel"]["distance"].value_or(8));
	Handles.P->GenChunks();


	glUniform1ui(Handles.TextureSampID, 0); //Set texturesampler

	//player.computeMatricesFromInputs(window); //Pre compute matrices

	//Set sky colour
	glClearColor(0.5f, 0.8f, 1,1);

	/*	Workaround for callbacks, since they need to be static
		Not /as/ bad as the previous workaround :)
	*/
	glfwSetWindowUserPointer(Handles.window, (void *)Handles.P);
	glfwSetScrollCallback(Handles.window, mouse_scroll_callback);
	glfwSetMouseButtonCallback(Handles.window, mouse_button_callback);

	// Update Loop
	do{
		switch(Handles.State) {
			case STATE_INIT:

			case STATE_LOADING:
				Loading(&Handles);
				break;
			case STATE_RUNNING:
				Running(&Handles);
				break;
		}
		// Swap buffers
		glfwSwapBuffers(Handles.window);
		glfwPollEvents();
	}

	// Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(Handles.window, GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwWindowShouldClose(Handles.window) == 0 );

	printf("Stopping...\n");

	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	//Free all the memory
	delete Handles.W;
	delete Handles.G;
	delete Handles.P;
	delete Generator; //fuck
	delete Persistance;

	glDeleteProgram(Handles.programID);
	glDeleteProgram(Handles.WireFrame);
	glDeleteTextures(1, &Handles.Texture);

	// Close OpenGL window and terminate GLFW
	glfwDestroyWindow(Handles.window);
	glfwTerminate();
	return 0;
}
