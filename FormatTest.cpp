#include "world/chunksource/storage/ucFormat/ucFormat.hpp"
#include <string.h>

#define CREATE
#define OPEN

int main(char argc, char** argv) {
	if(argc < 2)
		return -1;

	ucFile* test;
	ChunkRef chunk;
	chunk.Position = ChunkCoord(42, 42, 42);

	#ifdef OPEN
	if(strcmp(argv[1], "O") == 0) {
		// Open existing file
		test = new ucFile("test.uc");
		printf("Parsed %lu Chunks\n", test->GetNChunks());

		if(!test->GetChunk(&chunk)) {
			printf("Read failed :(\n");
			delete test;
			delete chunk.ChunkPointer;
			return -1;
		}

		printf("Data: %s\n", chunk.ChunkPointer->Voxels);

		memcpy((char*)chunk.ChunkPointer->Voxels, "Wheee", 6); // Breaks here
		test->SaveChunk(chunk);
		printf("Saved\n");

		delete chunk.ChunkPointer;

		chunk.ChunkPointer = new Chunk(test->ChunkSize, test->VoxelScale, ChunkCoord(0, 0, 0));
		chunk.Position = ChunkCoord(42, 42, 42);
		memcpy((char*)chunk.ChunkPointer->Voxels, "Woo!", 4);
		test->SaveChunk(chunk);

		delete chunk.ChunkPointer;
		chunk.Position = ChunkCoord(42, 42, 42);
		if(!test->GetChunk(&chunk))
			return -1;
		printf("Data: %s\n", chunk.ChunkPointer->Voxels);

		delete chunk.ChunkPointer;

		ChunkRef a;
		a.Position = ChunkCoord(42, 42, 42);

		printf("Data: %d\n", test->ChunkExists(&a));
		// printf("Data: %d\n", test->ChunkExists(ChunkCoord(42, 41, 42)));

		delete test;
	}

	#endif

	// Create new file
	#ifdef CREATE
	if(strcmp(argv[1], "C") == 0) {
		test = new ucFile("test.uc", 0x1000, 16+2, 1.0f, 0x100);

		chunk.ChunkPointer = new Chunk(test->ChunkSize, test->VoxelScale, ChunkCoord(0, 0, 0));
		chunk.Position = ChunkCoord(42, 42, 42);
		memcpy((char*)chunk.ChunkPointer->Voxels, "Woo", 4);
		test->SaveChunk(chunk);

		delete chunk.ChunkPointer;
		delete test;
	}
	#endif
}
